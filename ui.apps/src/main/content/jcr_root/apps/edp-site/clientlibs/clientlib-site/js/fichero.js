var imagen = {
	cargadas : [],
	contorno : 100,
	ampliar : function(a) {
		if ($.colorbox) {
			imagen.ampliarColorbox(a)
		} else {
			imagen.ampliarCapa(a)
		}
	},
	ampliarCapa : function(a) {
		var c = a.id.split("_")[1];
		var b = util.unicodeEscape(a.alt);
		if ($("body #adjunto")) {
			$("body #adjunto").remove()
		}
		$("body").append("<div id='adjunto'></div>");
		$("body #adjunto").append("<div id='precarga'></div>");
		$("body #adjunto #precarga").center();
		$.post(contextoPortal + "/" + codigoIdioma + "/cargarImagenAdjunta.do",
				{
					identificador : c,
					descripcion : b
				}, function(e) {
					$("body #adjunto").css({
						position : "absolute",
						left : "0",
						top : "0",
						visibility : "hidden"
					});
					$("body #adjunto").append(e);
					if (jQuery.inArray($("#imagenAdjunta").attr("src"),
							imagen.cargadas) > -1) {
						$("body #precarga").remove();
						$("body #adjunto").width(
								$("#imagenAdjunta").width() + imagen.contorno);
						$("body #adjunto").center();
						$("body #adjunto").css({
							visibility : "visible"
						})
					} else {
						$("#imagenAdjunta").load(
								function() {
									imagen.cargadas.push($("#imagenAdjunta")
											.attr("src"));
									$("body #precarga").remove();
									$("body #adjunto").width(
											$("#imagenAdjunta").width()
													+ imagen.contorno);
									$("body #a_ampliarImagen").width(
											$("#imagenAdjunta").width());
									$("body #adjunto").center();
									$("body #adjunto").css({
										visibility : "visible"
									})
								})
					}
					var d = decodeURIComponent($("body #adjunto p.piefoto")
							.text());
					if (typeof texto.altPopupImagen != "undefined") {
						d = d.replace(texto.altPopupImagen, "")
					}
					$("#imagenAdjunta").attr("alt", d);
					$("body #adjunto a").removeAttr("href");
					$("body #adjunto a").css({
						cursor : "pointer"
					});
					$("body #adjunto p.piefoto").text(d);
					$("body #adjunto").click(function() {
						imagen.reducir()
					})
				})
	},
	ampliarColorbox : function(b) {
		var f = b.src;
		var c = b.alt;
		if (typeof texto.altPopupImagen != "undefined") {
			c = c.replace(texto.altPopupImagen, "")
		}
		var a = f.substring(f.lastIndexOf("/") + 1, f.lastIndexOf("-mini-"));
		var e = f.substring(f.lastIndexOf("."));
		var d = f.substring(0, f.lastIndexOf("/") + 1) + a + e;
		$.colorbox({
			href : d,
			title : c,
			opacity : "0.5",
			maxWidth : "80%",
			maxHeight : "80%",
			rel : "editor",
			current : "{current} / {total}"
		})
	},
	reducir : function() {
		$("body #adjunto").remove()
	}
};