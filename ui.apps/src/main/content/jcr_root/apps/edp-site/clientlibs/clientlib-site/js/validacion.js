function validateDate(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oDate = eval("new " + jcv_retrieveFormName(form) + "_DateValidations()");
	for ( var x in oDate) {
		if (!jcv_verifyArrayElement(x, oDate[x])) {
			continue
		}
		var field = form[oDate[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		var value = field.value;
		var isStrict = true;
		var datePattern = oDate[x][2]("datePatternStrict");
		if (datePattern == null) {
			datePattern = oDate[x][2]("datePattern");
			isStrict = false
		}
		if ((field.type == "hidden" || field.type == "text" || field.type == "textarea")
				&& (value.length > 0) && (datePattern.length > 0)) {
			var MONTH = "MM";
			var DAY = "dd";
			var YEAR = "yyyy";
			var orderMonth = datePattern.indexOf(MONTH);
			var orderDay = datePattern.indexOf(DAY);
			var orderYear = datePattern.indexOf(YEAR);
			if ((orderDay < orderYear && orderDay > orderMonth)) {
				var iDelim1 = orderMonth + MONTH.length;
				var iDelim2 = orderDay + DAY.length;
				var delim1 = datePattern.substring(iDelim1, iDelim1 + 1);
				var delim2 = datePattern.substring(iDelim2, iDelim2 + 1);
				if (iDelim1 == orderDay && iDelim2 == orderYear) {
					dateRegexp = isStrict ? new RegExp(
							"^(\\d{2})(\\d{2})(\\d{4})$") : new RegExp(
							"^(\\d{1,2})(\\d{1,2})(\\d{4})$")
				} else {
					if (iDelim1 == orderDay) {
						dateRegexp = isStrict ? new RegExp("^(\\d{2})(\\d{2})["
								+ delim2 + "](\\d{4})$") : new RegExp(
								"^(\\d{1,2})(\\d{1,2})[" + delim2
										+ "](\\d{4})$")
					} else {
						if (iDelim2 == orderYear) {
							dateRegexp = isStrict ? new RegExp("^(\\d{2})["
									+ delim1 + "](\\d{2})(\\d{4})$")
									: new RegExp("^(\\d{1,2})[" + delim1
											+ "](\\d{1,2})(\\d{4})$")
						} else {
							dateRegexp = isStrict ? new RegExp("^(\\d{2})["
									+ delim1 + "](\\d{2})[" + delim2
									+ "](\\d{4})$") : new RegExp("^(\\d{1,2})["
									+ delim1 + "](\\d{1,2})[" + delim2
									+ "](\\d{4})$")
						}
					}
				}
				var matched = dateRegexp.exec(value);
				if (matched != null) {
					if (!jcv_isValidDate(matched[2], matched[1], matched[3])) {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oDate[x][1];
						bValid = false
					}
				} else {
					if (i == 0) {
						focusField = field
					}
					fields[i++] = oDate[x][1];
					bValid = false
				}
			} else {
				if ((orderMonth < orderYear && orderMonth > orderDay)) {
					var iDelim1 = orderDay + DAY.length;
					var iDelim2 = orderMonth + MONTH.length;
					var delim1 = datePattern.substring(iDelim1, iDelim1 + 1);
					var delim2 = datePattern.substring(iDelim2, iDelim2 + 1);
					if (iDelim1 == orderMonth && iDelim2 == orderYear) {
						dateRegexp = isStrict ? new RegExp(
								"^(\\d{2})(\\d{2})(\\d{4})$") : new RegExp(
								"^(\\d{1,2})(\\d{1,2})(\\d{4})$")
					} else {
						if (iDelim1 == orderMonth) {
							dateRegexp = isStrict ? new RegExp(
									"^(\\d{2})(\\d{2})[" + delim2
											+ "](\\d{4})$") : new RegExp(
									"^(\\d{1,2})(\\d{1,2})[" + delim2
											+ "](\\d{4})$")
						} else {
							if (iDelim2 == orderYear) {
								dateRegexp = isStrict ? new RegExp("^(\\d{2})["
										+ delim1 + "](\\d{2})(\\d{4})$")
										: new RegExp("^(\\d{1,2})[" + delim1
												+ "](\\d{1,2})(\\d{4})$")
							} else {
								dateRegexp = isStrict ? new RegExp("^(\\d{2})["
										+ delim1 + "](\\d{2})[" + delim2
										+ "](\\d{4})$") : new RegExp(
										"^(\\d{1,2})[" + delim1
												+ "](\\d{1,2})[" + delim2
												+ "](\\d{4})$")
							}
						}
					}
					var matched = dateRegexp.exec(value);
					if (matched != null) {
						if (!jcv_isValidDate(matched[1], matched[2], matched[3])) {
							if (i == 0) {
								focusField = field
							}
							fields[i++] = oDate[x][1];
							bValid = false
						}
					} else {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oDate[x][1];
						bValid = false
					}
				} else {
					if ((orderMonth > orderYear && orderMonth < orderDay)) {
						var iDelim1 = orderYear + YEAR.length;
						var iDelim2 = orderMonth + MONTH.length;
						var delim1 = datePattern
								.substring(iDelim1, iDelim1 + 1);
						var delim2 = datePattern
								.substring(iDelim2, iDelim2 + 1);
						if (iDelim1 == orderMonth && iDelim2 == orderDay) {
							dateRegexp = isStrict ? new RegExp(
									"^(\\d{4})(\\d{2})(\\d{2})$") : new RegExp(
									"^(\\d{4})(\\d{1,2})(\\d{1,2})$")
						} else {
							if (iDelim1 == orderMonth) {
								dateRegexp = isStrict ? new RegExp(
										"^(\\d{4})(\\d{2})[" + delim2
												+ "](\\d{2})$") : new RegExp(
										"^(\\d{4})(\\d{1,2})[" + delim2
												+ "](\\d{1,2})$")
							} else {
								if (iDelim2 == orderDay) {
									dateRegexp = isStrict ? new RegExp(
											"^(\\d{4})[" + delim1
													+ "](\\d{2})(\\d{2})$")
											: new RegExp("^(\\d{4})[" + delim1
													+ "](\\d{1,2})(\\d{1,2})$")
								} else {
									dateRegexp = isStrict ? new RegExp(
											"^(\\d{4})[" + delim1
													+ "](\\d{2})[" + delim2
													+ "](\\d{2})$")
											: new RegExp("^(\\d{4})[" + delim1
													+ "](\\d{1,2})[" + delim2
													+ "](\\d{1,2})$")
								}
							}
						}
						var matched = dateRegexp.exec(value);
						if (matched != null) {
							if (!jcv_isValidDate(matched[3], matched[2],
									matched[1])) {
								if (i == 0) {
									focusField = field
								}
								fields[i++] = oDate[x][1];
								bValid = false
							}
						} else {
							if (i == 0) {
								focusField = field
							}
							fields[i++] = oDate[x][1];
							bValid = false
						}
					} else {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oDate[x][1];
						bValid = false
					}
				}
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function jcv_isValidDate(b, d, c) {
	if (d < 1 || d > 12) {
		return false
	}
	if (b < 1 || b > 31) {
		return false
	}
	if ((d == 4 || d == 6 || d == 9 || d == 11) && (b == 31)) {
		return false
	}
	if (d == 2) {
		var a = (c % 4 == 0 && (c % 100 != 0 || c % 400 == 0));
		if (b > 29 || (b == 29 && !a)) {
			return false
		}
	}
	return true
}
function validateMinLength(form) {
	var isValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oMinLength = eval("new " + jcv_retrieveFormName(form) + "_minlength()");
	for ( var x in oMinLength) {
		if (!jcv_verifyArrayElement(x, oMinLength[x])) {
			continue
		}
		var field = form[oMinLength[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text"
				|| field.type == "password" || field.type == "textarea")) {
			var lineEndLength = oMinLength[x][2]("lineEndLength");
			var adjustAmount = 0;
			if (lineEndLength) {
				var rCount = 0;
				var nCount = 0;
				var crPos = 0;
				while (crPos < field.value.length) {
					var currChar = field.value.charAt(crPos);
					if (currChar == "\r") {
						rCount++
					}
					if (currChar == "\n") {
						nCount++
					}
					crPos++
				}
				var endLength = parseInt(lineEndLength);
				adjustAmount = (nCount * endLength) - (rCount + nCount)
			}
			var iMin = parseInt(oMinLength[x][2]("minlength"));
			if ((trim(field.value).length > 0)
					&& ((field.value.length + adjustAmount) < iMin)) {
				if (i == 0) {
					focusField = field
				}
				fields[i++] = oMinLength[x][1];
				isValid = false
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return isValid
}
function validateInteger(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oInteger = eval("new " + jcv_retrieveFormName(form)
			+ "_IntegerValidations()");
	for ( var x in oInteger) {
		if (!jcv_verifyArrayElement(x, oInteger[x])) {
			continue
		}
		var field = form[oInteger[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text"
				|| field.type == "textarea" || field.type == "select-one" || field.type == "radio")) {
			var value = "";
			if (field.type == "select-one") {
				var si = field.selectedIndex;
				if (si >= 0) {
					value = field.options[si].value
				}
			} else {
				value = field.value
			}
			if (value.length > 0) {
				if (!jcv_isDecimalDigits(value)) {
					bValid = false;
					if (i == 0) {
						focusField = field
					}
					fields[i++] = oInteger[x][1]
				} else {
					var iValue = parseInt(value, 10);
					if (isNaN(iValue)
							|| !(iValue >= -2147483648 && iValue <= 2147483647)) {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oInteger[x][1];
						bValid = false
					}
				}
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function validateFloat(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oFloat = eval("new " + jcv_retrieveFormName(form)
			+ "_FloatValidations()");
	for ( var x in oFloat) {
		if (!jcv_verifyArrayElement(x, oFloat[x])) {
			continue
		}
		var field = form[oFloat[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text"
				|| field.type == "textarea" || field.type == "select-one" || field.type == "radio")) {
			var value = "";
			if (field.type == "select-one") {
				var si = field.selectedIndex;
				if (si >= 0) {
					value = field.options[si].value
				}
			} else {
				value = field.value
			}
			if (value.length > 0) {
				var tempArray = value.split(".");
				var zeroIndex = 0;
				var joinedString = tempArray.join("");
				while (joinedString.charAt(zeroIndex) == "0") {
					zeroIndex++
				}
				var noZeroString = joinedString.substring(zeroIndex,
						joinedString.length);
				if (!jcv_isAllDigits(noZeroString) || tempArray.length > 2) {
					bValid = false;
					if (i == 0) {
						focusField = field
					}
					fields[i++] = oFloat[x][1]
				} else {
					var iValue = parseFloat(value);
					if (isNaN(iValue)) {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oFloat[x][1];
						bValid = false
					}
				}
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function validateRequired(form) {
	var isValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oRequired = eval("new " + jcv_retrieveFormName(form) + "_required()");
	for ( var x in oRequired) {
		if (!jcv_verifyArrayElement(x, oRequired[x])) {
			continue
		}
		var field = form[oRequired[x][0]];
		if (!jcv_isFieldPresent(field)) {
			fields[i++] = oRequired[x][1];
			isValid = false
		} else {
			if ((field.type == "hidden" || field.type == "text"
					|| field.type == "textarea" || field.type == "file"
					|| field.type == "radio" || field.type == "checkbox"
					|| field.type == "select-one" || field.type == "password")) {
				var value = "";
				if (field.type == "select-one") {
					var si = field.selectedIndex;
					if (si >= 0) {
						value = field.options[si].value
					}
				} else {
					if (field.type == "radio" || field.type == "checkbox") {
						if (field.checked) {
							value = field.value
						}
					} else {
						value = field.value
					}
				}
				if (trim(value).length == 0) {
					if ((i == 0) && (field.type != "hidden")) {
						focusField = field
					}
					fields[i++] = oRequired[x][1];
					isValid = false
				}
			} else {
				if (field.type == "select-multiple") {
					var numOptions = field.options.length;
					lastSelected = -1;
					for (loop = numOptions - 1; loop >= 0; loop--) {
						if (field.options[loop].selected) {
							lastSelected = loop;
							value = field.options[loop].value;
							break
						}
					}
					if (lastSelected < 0 || trim(value).length == 0) {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oRequired[x][1];
						isValid = false
					}
				} else {
					if ((field.length > 0)
							&& (field[0].type == "radio" || field[0].type == "checkbox")) {
						isChecked = -1;
						for (loop = 0; loop < field.length; loop++) {
							if (field[loop].checked) {
								isChecked = loop;
								break
							}
						}
						if (isChecked < 0) {
							if (i == 0) {
								focusField = field[0]
							}
							fields[i++] = oRequired[x][1];
							isValid = false
						}
					}
				}
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return isValid
}
function trim(a) {
	return a.replace(/^\s*/, "").replace(/\s*$/, "")
}
function jcv_retrieveFormName(b) {
	var a;
	if (b.getAttributeNode) {
		if (b.getAttributeNode("id") && b.getAttributeNode("id").value) {
			a = b.getAttributeNode("id").value
		} else {
			a = b.getAttributeNode("name").value
		}
	} else {
		if (b.getAttribute) {
			if (b.getAttribute("id")) {
				a = b.getAttribute("id")
			} else {
				a = b.attributes.name
			}
		} else {
			if (b.id) {
				a = b.id
			} else {
				a = b.name
			}
		}
	}
	return a
}
function jcv_handleErrors(c, b) {
	if (b && b != null) {
		var a = true;
		if (b.disabled || b.type == "hidden") {
			a = false
		}
		if (a && b.style && b.style.visibility
				&& b.style.visibility == "hidden") {
			a = false
		}
		if (a) {
			b.focus()
		}
	}
	alert(c.join("\n"))
}
function jcv_verifyArrayElement(a, b) {
	if (b && b.length && b.length == 3) {
		return true
	} else {
		return false
	}
}
function jcv_isFieldPresent(b) {
	var a = true;
	if (b == null || (typeof b == "undefined")) {
		a = false
	} else {
		if (b.disabled) {
			a = false
		}
	}
	return a
}
function jcv_isAllDigits(b) {
	b = b.toString();
	var a = "0123456789";
	var c = 0;
	if (b.substring(0, 2) == "0x") {
		a = "0123456789abcdefABCDEF";
		c = 2
	} else {
		if (b.charAt(0) == "0") {
			a = "01234567";
			c = 1
		} else {
			if (b.charAt(0) == "-") {
				c = 1
			}
		}
	}
	for (var d = c; d < b.length; d++) {
		if (a.indexOf(b.substring(d, d + 1)) == -1) {
			return false
		}
	}
	return true
}
function jcv_isDecimalDigits(b) {
	b = b.toString();
	var a = "0123456789";
	var c = 0;
	if (b.charAt(0) == "-") {
		c = 1
	}
	for (var d = c; d < b.length; d++) {
		if (a.indexOf(b.substring(d, d + 1)) == -1) {
			return false
		}
	}
	return true
}
function validateEmail(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oEmail = eval("new " + jcv_retrieveFormName(form) + "_email()");
	for ( var x in oEmail) {
		if (!jcv_verifyArrayElement(x, oEmail[x])) {
			continue
		}
		var field = form[oEmail[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text" || field.type == "textarea")
				&& (field.value.length > 0)) {
			if (!jcv_checkEmail(field.value)) {
				if (i == 0) {
					focusField = field
				}
				fields[i++] = oEmail[x][1];
				bValid = false
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function jcv_checkEmail(b) {
	if (b.length == 0) {
		return true
	}
	var r = 0;
	var u = /^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
	var q = /^(.+)@(.+)$/;
	var o = '\\(\\)><@,;:\\\\\\"\\.\\[\\]';
	var j = "[^\\s" + o + "]";
	var d = '("[^"]*")';
	var p = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var a = j + "+";
	var n = "(" + a + "|" + d + ")";
	var k = new RegExp("^" + n + "(\\." + n + ")*$");
	var f = new RegExp("^" + a + "(\\." + a + ")*$");
	var c = b.match(q);
	if (c == null) {
		return false
	}
	var s = c[1];
	var t = c[2];
	for (l = 0; l < s.length; l++) {
		if (s.charCodeAt(l) > 127) {
			return false
		}
	}
	for (l = 0; l < t.length; l++) {
		if (t.charCodeAt(l) > 127) {
			return false
		}
	}
	if (s.match(k) == null) {
		return false
	}
	var h = t.match(p);
	if (h != null) {
		for (var l = 1; l <= 4; l++) {
			if (h[l] > 255) {
				return false
			}
		}
		return true
	}
	var g = new RegExp("^" + a + "$");
	var e = t.split(".");
	var m = e.length;
	for (l = 0; l < m; l++) {
		if (e[l].search(g) == -1) {
			return false
		}
	}
	if (r && e[e.length - 1].length != 2 && e[e.length - 1].search(u) == -1) {
		return false
	}
	if (m < 2) {
		return false
	}
	return true
}
function validateMask(form) {
	var isValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oMasked = eval("new " + jcv_retrieveFormName(form) + "_mask()");
	for ( var x in oMasked) {
		if (!jcv_verifyArrayElement(x, oMasked[x])) {
			continue
		}
		var field = form[oMasked[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text"
				|| field.type == "textarea" || field.type == "file")
				&& (field.value.length > 0)) {
			if (!jcv_matchPattern(field.value, oMasked[x][2]("mask"))) {
				if (i == 0) {
					focusField = field
				}
				fields[i++] = oMasked[x][1];
				isValid = false
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return isValid
}
function jcv_matchPattern(b, a) {
	return a.exec(b)
}
function validateIntRange(form) {
	var isValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oRange = eval("new " + jcv_retrieveFormName(form) + "_intRange()");
	for ( var x in oRange) {
		if (!jcv_verifyArrayElement(x, oRange[x])) {
			continue
		}
		var field = form[oRange[x][0]];
		if (jcv_isFieldPresent(field)) {
			var value = "";
			if (field.type == "hidden" || field.type == "text"
					|| field.type == "textarea" || field.type == "radio") {
				value = field.value
			}
			if (field.type == "select-one") {
				var si = field.selectedIndex;
				if (si >= 0) {
					value = field.options[si].value
				}
			}
			if (value.length > 0) {
				var iMin = parseInt(oRange[x][2]("min"));
				var iMax = parseInt(oRange[x][2]("max"));
				var iValue = parseInt(value, 10);
				if (!(iValue >= iMin && iValue <= iMax)) {
					if (i == 0) {
						focusField = field
					}
					fields[i++] = oRange[x][1];
					isValid = false
				}
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return isValid
}
function validateMaxLength(form) {
	var isValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oMaxLength = eval("new " + jcv_retrieveFormName(form) + "_maxlength()");
	for ( var x in oMaxLength) {
		if (!jcv_verifyArrayElement(x, oMaxLength[x])) {
			continue
		}
		var field = form[oMaxLength[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text"
				|| field.type == "password" || field.type == "textarea")) {
			var lineEndLength = oMaxLength[x][2]("lineEndLength");
			var adjustAmount = 0;
			if (lineEndLength) {
				var rCount = 0;
				var nCount = 0;
				var crPos = 0;
				while (crPos < field.value.length) {
					var currChar = field.value.charAt(crPos);
					if (currChar == "\r") {
						rCount++
					}
					if (currChar == "\n") {
						nCount++
					}
					crPos++
				}
				var endLength = parseInt(lineEndLength);
				adjustAmount = (nCount * endLength) - (rCount + nCount)
			}
			var iMax = parseInt(oMaxLength[x][2]("maxlength"));
			if ((field.value.length + adjustAmount) > iMax) {
				if (i == 0) {
					focusField = field
				}
				fields[i++] = oMaxLength[x][1];
				isValid = false
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return isValid
}
function validateFloatRange(form) {
	var isValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oRange = eval("new " + jcv_retrieveFormName(form) + "_floatRange()");
	for ( var x in oRange) {
		if (!jcv_verifyArrayElement(x, oRange[x])) {
			continue
		}
		var field = form[oRange[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text" || field.type == "textarea")
				&& (field.value.length > 0)) {
			var fMin = parseFloat(oRange[x][2]("min"));
			var fMax = parseFloat(oRange[x][2]("max"));
			var fValue = parseFloat(field.value);
			if (!(fValue >= fMin && fValue <= fMax)) {
				if (i == 0) {
					focusField = field
				}
				fields[i++] = oRange[x][1];
				isValid = false
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return isValid
}
function validateByte(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oByte = eval("new " + jcv_retrieveFormName(form) + "_ByteValidations()");
	for ( var x in oByte) {
		if (!jcv_verifyArrayElement(x, oByte[x])) {
			continue
		}
		var field = form[oByte[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text"
				|| field.type == "textarea" || field.type == "select-one" || field.type == "radio")) {
			var value = "";
			if (field.type == "select-one") {
				var si = field.selectedIndex;
				if (si >= 0) {
					value = field.options[si].value
				}
			} else {
				value = field.value
			}
			if (value.length > 0) {
				if (!jcv_isDecimalDigits(value)) {
					bValid = false;
					if (i == 0) {
						focusField = field
					}
					fields[i++] = oByte[x][1]
				} else {
					var iValue = parseInt(value, 10);
					if (isNaN(iValue) || !(iValue >= -128 && iValue <= 127)) {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oByte[x][1];
						bValid = false
					}
				}
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function validateFilterXSS(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oFilterXSS = eval("new " + jcv_retrieveFormName(form) + "_filterXSS()");
	for ( var x in oFilterXSS) {
		if (!jcv_verifyArrayElement(x, oFilterXSS[x])) {
			continue
		}
		var field = form[oFilterXSS[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text" || field.type == "textarea")
				&& (field.value.length > 0)) {
			if (!jcv_checkFilterXSS(field.value)) {
				if (i == 0) {
					focusField = field
				}
				fields[i++] = oFilterXSS[x][1];
				bValid = false
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function jcv_checkFilterXSS(b) {
	if (b.length == 0) {
		return true
	}
	var a = /^[A-Za-z0-9@\u00C0-\u0233\u0370-\u03FF\u0600-\u06FF\u20A0-\u20CF\u3040-\u309F\u30A0-\u30FF\u002D\u00AA\u00BA@!\"\\\\#$%&'*+,/:;=\\.?@_\\`{}\\~\\-\\^ (\r\n|\r|\n)]*$/;
	var c = b.match(a);
	if (c == null) {
		return false
	}
	return true
}
function validateShort(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oShort = eval("new " + jcv_retrieveFormName(form)
			+ "_ShortValidations()");
	for ( var x in oShort) {
		if (!jcv_verifyArrayElement(x, oShort[x])) {
			continue
		}
		var field = form[oShort[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "hidden" || field.type == "text"
				|| field.type == "textarea" || field.type == "select-one" || field.type == "radio")) {
			var value = "";
			if (field.type == "select-one") {
				var si = field.selectedIndex;
				if (si >= 0) {
					value = field.options[si].value
				}
			} else {
				value = field.value
			}
			if (value.length > 0) {
				if (!jcv_isDecimalDigits(value)) {
					bValid = false;
					if (i == 0) {
						focusField = field
					}
					fields[i++] = oShort[x][1]
				} else {
					var iValue = parseInt(value, 10);
					if (isNaN(iValue) || !(iValue >= -32768 && iValue <= 32767)) {
						if (i == 0) {
							focusField = field
						}
						fields[i++] = oShort[x][1];
						bValid = false
					}
				}
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function validateCreditCard(form) {
	var bValid = true;
	var focusField = null;
	var i = 0;
	var fields = new Array();
	var oCreditCard = eval("new " + jcv_retrieveFormName(form)
			+ "_creditCard()");
	for ( var x in oCreditCard) {
		if (!jcv_verifyArrayElement(x, oCreditCard[x])) {
			continue
		}
		var field = form[oCreditCard[x][0]];
		if (!jcv_isFieldPresent(field)) {
			continue
		}
		if ((field.type == "text" || field.type == "textarea")
				&& (field.value.length > 0)) {
			if (!jcv_luhnCheck(field.value)) {
				if (i == 0) {
					focusField = field
				}
				fields[i++] = oCreditCard[x][1];
				bValid = false
			}
		}
	}
	if (fields.length > 0) {
		jcv_handleErrors(fields, focusField)
	}
	return bValid
}
function jcv_luhnCheck(e) {
	if (jcv_isLuhnNum(e)) {
		var a = e.length;
		var b = a & 1;
		var c = 0;
		for (var d = 0; d < a; d++) {
			var f = parseInt(e.charAt(d));
			if (!((d & 1) ^ b)) {
				f *= 2;
				if (f > 9) {
					f -= 9
				}
			}
			c += f
		}
		if (c == 0) {
			return false
		}
		if (c % 10 == 0) {
			return true
		}
	}
	return false
}
function jcv_isLuhnNum(a) {
	a = a.toString();
	if (a.length == 0) {
		return false
	}
	for (var b = 0; b < a.length; b++) {
		if ((a.substring(b, b + 1) < "0") || (a.substring(b, b + 1) > "9")) {
			return false
		}
	}
	return true
};