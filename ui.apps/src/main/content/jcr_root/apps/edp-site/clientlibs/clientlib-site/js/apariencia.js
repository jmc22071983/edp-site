
var apariencia = {
	modoPc : false,
	modoPrevio : this.modoPc,
	inicializar : function() {
		apariencia.ajustarApariencia();
		apariencia.accionesCabecera();
		apariencia.accionScroll();
		apariencia.atajo();
		apariencia.igualaAlto();
		apariencia.animacionAnclas();
		apariencia.cargarAcordeon();
		apariencia.cargarTabs();
		apariencia.previsCms();
		$(window).resize(function() {
			apariencia.ajustarApariencia();
			apariencia.igualaAlto()
		})
	},
	ajustarApariencia : function() {
		if ($(window).width() >= 1080) {
			apariencia.modoPc = true
		} else {
			apariencia.modoPc = false
		}
		if (apariencia.modoPrevio != apariencia.modoPc) {
			apariencia.estiloPc()
		}
		if ($(window).width() >= 750) {
			if ($(".acordeon-device").hasClass("ui-accordion")) {
				$(".acordeon-device").accordion("destroy")
			}
		}
		apariencia.modoPrevio = apariencia.modoPc
	},
	accionesCabecera : function() {
		$("#ver-menu").on("click", function() {
			$("body").addClass("nav-on").scrollTop("0");
			apariencia.navMovil()
		});
		$("#cerrar-menu").on("click", function() {
			$("body").removeClass("nav-on");
			$("#sec-nav .n2").removeClass('[class$="on"]');
			$("#sec-nav ul").removeClass("on");
			$("#sec-nav li").removeClass("menu-on");
			$(".enlace-padre").remove()
		});
		$("#ver-buscador").on("click", function() {
			$("#buscadorGeneralForm").toggleClass("buscador-on")
		});
		$("#ver-idioma").on("click", function() {
			$("#idioma").toggleClass("idioma-on")
		})
	},
	accionScroll : function() {
		function a() {
			if ($(this).scrollTop() > 0) {
				$("body").addClass("scroll-on")
			} else {
				$("body").removeClass("scroll-on");
				$("body").removeClass("buscador-on")
			}
		}
		a();
		$(window).scroll(function() {
			a()
		})
	},
	atajo : function() {
		if ($("#atajo").length > 0) {
			$("#atajo .subir a").click(function() {
				$("body,html").animate({
					scrollTop : 0
				}, 500);
				return false
			});
			$("#atajo .compartir").on({
				mouseenter : function() {
					$(".compartir").addClass("on")
				},
				mouseleave : function() {
					$(".compartir").removeClass("on")
				}
			})
		}
	},
	igualaAlto : function() {
	},
	igualarAlto : function(a) {
		masAlto = 0;
		a.each(function() {
			alto = $(this).height();
			if (this.height > masAlto) {
				masAlto = alto
			}
		});
		a.height(masAlto)
	},
	igualarAltoLinea : function(e) {
		var a = 0, b = 0, f = new Array(), d, c = 0;
		$(e).each(function() {
			d = $(this);
			$(d).height("auto");
			topPostion = d.position().top;
			if (b != topPostion) {
				for (currentDiv = 0; currentDiv < f.length; currentDiv++) {
					f[currentDiv].height(a)
				}
				f.length = 0;
				b = topPostion;
				a = d.height();
				f.push(d)
			} else {
				f.push(d);
				a = (a < d.height()) ? (d.height()) : (a)
			}
			for (currentDiv = 0; currentDiv < f.length; currentDiv++) {
				f[currentDiv].height(a)
			}
		})
	},
	animacionAnclas : function() {
		$('a[href^="#"]').not(":has(span)").click(function() {
			var a = $(this).attr("href");
			if (a && a.length > 1) {
				var d = $(a).offset().top;
				var c = $("#main-header").height();
				var b = d - c - 80;
				$("body,html").animate({
					scrollTop : b
				}, 500);
				return false
			}
		});
		if (window.location.hash) {
			setTimeout(function() {
				var a = $("#main-header").height();
				var b = $(window.location.hash).offset().top - a - 30;
				$("html,body").animate({
					scrollTop : b
				}, 500)
			}, 500)
		}
	},
	cargarTabs : function() {
		if ($(".pestanas").length) {
			$(".pestanas").responsiveTabs()
		}
		if ($(".pestanas-home").length) {
			$(".pestanas-home").responsiveTabs(
					{
						
					})
		}
	},
	cargarAcordeon : function() {
		if ($(".acordeon").length > 0) {
			$(".acordeon").accordion({
				header : "h2.titulo2, h3.titulo2",
				heightStyle : "content",
				collapsible : true,
				active : false,
				activate : function(a, b) {
					var c = b.newHeader.attr("id");
					if (c != undefined) {
						setTimeout(function() {
							var e = $("#" + c).offset().top;
							var d = $("#main-header").height() + 30;
							$("html, body").animate({
								scrollTop : e - d
							}, 500)
						}, 500)
					}
				}
			})
		}
	},
	cargarAcordeonDevice : function() {
		if ($(".acordeon-device").length > 0) {
			$(".acordeon-device").accordion({
				header : ".tit-acordeon",
				heightStyle : "content",
				collapsible : true,
				active : false
			})
		}
	},
	navMovil : function() {
		function a() {
			var b = $(this).parent();
			if ($(this).parent().hasClass("pmcon")) {
				if (!$(this).hasClass("menu-on")) {
					event.preventDefault();
					$(this).addClass("enlace-on");
					$(this).off("click");
					$(b).addClass("menu-on");
					$(this).next("ul").addClass("on");
					$("#sec-nav .n2").addClass("sec-on")
				}
			}
		}
		$("#sec-nav .n2 a").on("click", a);
		$(".volver-nav").on("click", function() {
			$("ul").removeClass("on");
			$("#sec-nav .n2 a").on("click", a);
			setTimeout(function() {
				$(".sec-on").removeClass("sec-on");
				$(".enlace-on").removeClass("enlace-on");
				$(".menu-on").removeClass("menu-on")
			}, 500)
		})
	},
	previsCms : function() {
		if ($("body").parents("iframe")) {
		}
	},
	estiloPc : function() {
		if (apariencia.modoPc) {
		} else {
			apariencia.navMovil()
		}
	}
};
function is_touch_device() {
	return (("ontouchstart" in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0))
}
if (!is_touch_device()) {
	//portal.cargarTooltip()
};