(function(c, b, e) {
	var d = {
		active : null,
		event : "click",
		disabled : [],
		collapsible : "accordion",
		startCollapsed : false,
		rotate : false,
		setHash : false,
		animation : "default",
		animationQueue : false,
		duration : 500,
		fluidHeight : true,
		scrollToAccordion : false,
		scrollToAccordionOnLoad : true,
		scrollToAccordionOffset : 0,
		accordionTabElement : "<div></div>",
		navigationContainer : "",
		click : function() {
		},
		activate : function() {
		},
		deactivate : function() {
		},
		load : function() {
		},
		activateState : function() {
		},
		classes : {
			stateDefault : "r-tabs-state-default",
			stateActive : "r-tabs-state-active",
			stateDisabled : "r-tabs-state-disabled",
			stateExcluded : "r-tabs-state-excluded",
			container : "r-tabs",
			ul : "r-tabs-nav",
			tab : "r-tabs-tab",
			anchor : "r-tabs-anchor",
			panel : "r-tabs-panel",
			accordionTitle : "r-tabs-accordion-title"
		}
	};
	function a(g, f) {
		this.element = g;
		this.$element = c(g);
		this.tabs = [];
		this.state = "";
		this.rotateInterval = 0;
		this.$queue = c({});
		this.options = c.extend({}, d, f);
		this.init()
	}
	a.prototype.init = function() {
		var f = this;
		this.tabs = this._loadElements();
		this._loadClasses();
		this._loadEvents();
		c(b).on("resize", function(g) {
			f._setState(g);
			if (f.options.fluidHeight !== true) {
				f._equaliseHeights()
			}
		});
		c(b).on("hashchange", function(h) {
			var g = f._getTabRefBySelector(b.location.hash);
			var i = f._getTab(g);
			if (g >= 0 && !i._ignoreHashChange && !i.disabled) {
				f._openTab(h, f._getTab(g), true)
			}
		});
		if (this.options.rotate !== false) {
			this.startRotation()
		}
		if (this.options.fluidHeight !== true) {
			f._equaliseHeights()
		}
		this.$element.bind("tabs-click", function(g, h) {
			f.options.click.call(this, g, h)
		});
		this.$element.bind("tabs-activate", function(g, h) {
			f.options.activate.call(this, g, h)
		});
		this.$element.bind("tabs-deactivate", function(g, h) {
			f.options.deactivate.call(this, g, h)
		});
		this.$element.bind("tabs-activate-state", function(h, g) {
			f.options.activateState.call(this, h, g)
		});
		this.$element
				.bind(
						"tabs-load",
						function(g) {
							var h;
							f._setState(g);
							if (f.options.startCollapsed !== true
									&& !(f.options.startCollapsed === "accordion" && f.state === "accordion")) {
								h = f._getStartTab();
								f._openTab(g, h);
								f.options.load.call(this, g, h)
							}
						});
		this.$element.trigger("tabs-load")
	};
	a.prototype._loadElements = function() {
		var i = this;
		var f = (i.options.navigationContainer === "") ? this.$element
				.children("ul:first") : this.$element.find(
				i.options.navigationContainer).children("ul:first");
		var g = [];
		var h = 0;
		this.$element.addClass(i.options.classes.container);
		f.addClass(i.options.classes.ul);
		c("li", f).each(function() {
			var m = c(this);
			var j = m.hasClass(i.options.classes.stateExcluded);
			var l, p, n, k, q;
			if (!j) {
				l = c("a", m);
				q = l.attr("href");
				p = c(q);
				n = c(i.options.accordionTabElement).insertBefore(p);
				k = c("<a></a>").attr("href", q).html(l.html()).appendTo(n);
				var o = {
					_ignoreHashChange : false,
					id : h,
					disabled : (c.inArray(h, i.options.disabled) !== -1),
					tab : c(this),
					anchor : c("a", m),
					panel : p,
					selector : q,
					accordionTab : n,
					accordionAnchor : k,
					active : false
				};
				h++;
				g.push(o)
			}
		});
		return g
	};
	a.prototype._loadClasses = function() {
		for (var f = 0; f < this.tabs.length; f++) {
			this.tabs[f].tab.addClass(this.options.classes.stateDefault)
					.addClass(this.options.classes.tab);
			this.tabs[f].anchor.addClass(this.options.classes.anchor);
			this.tabs[f].panel.addClass(this.options.classes.stateDefault)
					.addClass(this.options.classes.panel);
			this.tabs[f].accordionTab
					.addClass(this.options.classes.accordionTitle);
			this.tabs[f].accordionAnchor.addClass(this.options.classes.anchor);
			if (this.tabs[f].disabled) {
				this.tabs[f].tab.removeClass(this.options.classes.stateDefault)
						.addClass(this.options.classes.stateDisabled);
				this.tabs[f].accordionTab.removeClass(
						this.options.classes.stateDefault).addClass(
						this.options.classes.stateDisabled)
			}
		}
	};
	a.prototype._loadEvents = function() {
		var h = this;
		var f = function(j) {
			var i = h._getCurrentTab();
			var k = j.data.tab;
			j.preventDefault();
			k.tab.trigger("tabs-click", k);
			if (!k.disabled) {
				if (h.options.setHash) {
					if (history.pushState) {
						if (!b.location.origin) {
							b.location.origin = b.location.protocol
									+ "//"
									+ b.location.hostname
									+ (b.location.port ? ":" + b.location.port
											: "")
						}
						history.pushState(null, null, b.location.origin
								+ b.location.pathname + b.location.search
								+ k.selector)
					} else {
						b.location.hash = k.selector
					}
				}
				j.data.tab._ignoreHashChange = true;
				if (i !== k || h._isCollapisble()) {
					h._closeTab(j, i);
					if (i !== k || !h._isCollapisble()) {
						h._openTab(j, k, false, true)
					}
				}
			}
		};
		for (var g = 0; g < this.tabs.length; g++) {
			this.tabs[g].anchor.on(h.options.event, {
				tab : h.tabs[g]
			}, f);
			this.tabs[g].accordionAnchor.on(h.options.event, {
				tab : h.tabs[g]
			}, f)
		}
	};
	a.prototype._getStartTab = function() {
		var f = this._getTabRefBySelector(b.location.hash);
		var g;
		if (f >= 0 && !this._getTab(f).disabled) {
			g = this._getTab(f)
		} else {
			if (this.options.active > 0
					&& !this._getTab(this.options.active).disabled) {
				g = this._getTab(this.options.active)
			} else {
				g = this._getTab(0)
			}
		}
		return g
	};
	a.prototype._setState = function(i) {
		var g = c("ul:first", this.$element);
		var f = this.state;
		var h = (typeof this.options.startCollapsed === "string");
		var j;
		if (g.is(":visible")) {
			this.state = "tabs"
		} else {
			this.state = "accordion"
		}
		if (this.state !== f) {
			this.$element.trigger("tabs-activate-state", {
				oldState : f,
				newState : this.state
			});
			if (f && h && this.options.startCollapsed !== this.state
					&& this._getCurrentTab() === e) {
				j = this._getStartTab(i);
				this._openTab(i, j)
			}
		}
	};
	a.prototype._openTab = function(h, j, i, f) {
		var k = this;
		var g;
		if (i) {
			this._closeTab(h, this._getCurrentTab())
		}
		if (f && this.rotateInterval > 0) {
			this.stopRotation()
		}
		j.active = true;
		j.tab.removeClass(k.options.classes.stateDefault).addClass(
				k.options.classes.stateActive);
		j.accordionTab.removeClass(k.options.classes.stateDefault).addClass(
				k.options.classes.stateActive);
		k
				._doTransition(
						j.panel,
						k.options.animation,
						"open",
						function() {
							var l = (h.type !== "tabs-load" || k.options.scrollToAccordionOnLoad);
							j.panel.removeClass(k.options.classes.stateDefault)
									.addClass(k.options.classes.stateActive);
							if (k.getState() === "accordion"
									&& k.options.scrollToAccordion
									&& (!k._isInView(j.accordionTab) || k.options.animation !== "default")
									&& l) {
								g = j.accordionTab.offset().top
										- k.options.scrollToAccordionOffset;
								if (k.options.animation !== "default"
										&& k.options.duration > 0) {
									c("html, body").animate({
										scrollTop : g
									}, k.options.duration)
								} else {
									c("html, body").scrollTop(g)
								}
							}
						});
		this.$element.trigger("tabs-activate", j)
	};
	a.prototype._closeTab = function(h, i) {
		var j = this;
		var g = typeof j.options.animationQueue === "string";
		var f;
		if (i !== e) {
			if (g && j.getState() === j.options.animationQueue) {
				f = true
			} else {
				if (g) {
					f = false
				} else {
					f = j.options.animationQueue
				}
			}
			i.active = false;
			i.tab.removeClass(j.options.classes.stateActive).addClass(
					j.options.classes.stateDefault);
			j._doTransition(i.panel, j.options.animation, "close", function() {
				i.accordionTab.removeClass(j.options.classes.stateActive)
						.addClass(j.options.classes.stateDefault);
				i.panel.removeClass(j.options.classes.stateActive).addClass(
						j.options.classes.stateDefault)
			}, !f);
			this.$element.trigger("tabs-deactivate", i)
		}
	};
	a.prototype._doTransition = function(f, l, i, k, h) {
		var g;
		var j = this;
		switch (l) {
		case "slide":
			g = (i === "open") ? "slideDown" : "slideUp";
			break;
		case "fade":
			g = (i === "open") ? "fadeIn" : "fadeOut";
			break;
		default:
			g = (i === "open") ? "show" : "hide";
			j.options.duration = 0;
			break
		}
		this.$queue.queue("responsive-tabs", function(m) {
			f[g]({
				duration : j.options.duration,
				complete : function() {
					k.call(f, l, i);
					m()
				}
			})
		});
		if (i === "open" || h) {
			this.$queue.dequeue("responsive-tabs")
		}
	};
	a.prototype._isCollapisble = function() {
		return (typeof this.options.collapsible === "boolean" && this.options.collapsible)
				|| (typeof this.options.collapsible === "string" && this.options.collapsible === this
						.getState())
	};
	a.prototype._getTab = function(f) {
		return this.tabs[f]
	};
	a.prototype._getTabRefBySelector = function(f) {
		for (var g = 0; g < this.tabs.length; g++) {
			if (this.tabs[g].selector === f) {
				return g
			}
		}
		return -1
	};
	a.prototype._getCurrentTab = function() {
		return this._getTab(this._getCurrentTabRef())
	};
	a.prototype._getNextTabRef = function(g) {
		var f = (g || this._getCurrentTabRef());
		var h = (f === this.tabs.length - 1) ? 0 : f + 1;
		return (this._getTab(h).disabled) ? this._getNextTabRef(h) : h
	};
	a.prototype._getPreviousTabRef = function() {
		return (this._getCurrentTabRef() === 0) ? this.tabs.length - 1 : this
				._getCurrentTabRef() - 1
	};
	a.prototype._getCurrentTabRef = function() {
		for (var f = 0; f < this.tabs.length; f++) {
			if (this.tabs[f].active) {
				return f
			}
		}
		return -1
	};
	a.prototype._equaliseHeights = function() {
		var f = 0;
		c.each(c.map(this.tabs, function(g) {
			f = Math.max(f, g.panel.css("minHeight", "").height());
			return g.panel
		}), function() {
			this.css("minHeight", f)
		})
	};
	a.prototype._isInView = function(g) {
		var j = c(b).scrollTop(), i = j + c(b).height(), f = g.offset().top, h = f
				+ g.height();
		return ((h <= i) && (f >= j))
	};
	a.prototype.activate = function(g, f) {
		var h = jQuery.Event("tabs-activate");
		var i = this._getTab(g);
		if (!i.disabled) {
			this._openTab(h, i, true, f || true)
		}
	};
	a.prototype.deactivate = function(f) {
		var g = jQuery.Event("tabs-dectivate");
		var h = this._getTab(f);
		if (!h.disabled) {
			this._closeTab(g, h)
		}
	};
	a.prototype.enable = function(f) {
		var g = this._getTab(f);
		if (g) {
			g.disabled = false;
			g.tab.addClass(this.options.classes.stateDefault).removeClass(
					this.options.classes.stateDisabled);
			g.accordionTab.addClass(this.options.classes.stateDefault)
					.removeClass(this.options.classes.stateDisabled)
		}
	};
	a.prototype.disable = function(f) {
		var g = this._getTab(f);
		if (g) {
			g.disabled = true;
			g.tab.removeClass(this.options.classes.stateDefault).addClass(
					this.options.classes.stateDisabled);
			g.accordionTab.removeClass(this.options.classes.stateDefault)
					.addClass(this.options.classes.stateDisabled)
		}
	};
	a.prototype.getState = function() {
		return this.state
	};
	a.prototype.startRotation = function(f) {
		var g = this;
		if (this.tabs.length > this.options.disabled.length) {
			this.rotateInterval = setInterval(function() {
				var h = jQuery.Event("rotate");
				g._openTab(h, g._getTab(g._getNextTabRef()), true)
			}, f || ((c.isNumeric(g.options.rotate)) ? g.options.rotate : 4000))
		} else {
			throw new Error("Rotation is not possible if all tabs are disabled")
		}
	};
	a.prototype.stopRotation = function() {
		b.clearInterval(this.rotateInterval);
		this.rotateInterval = 0
	};
	a.prototype.option = function(f, g) {
		if (g) {
			this.options[f] = g
		}
		return this.options[f]
	};
	c.fn.responsiveTabs = function(h) {
		var g = arguments;
		var f;
		if (h === e || typeof h === "object") {
			return this.each(function() {
				if (!c.data(this, "responsivetabs")) {
					c.data(this, "responsivetabs", new a(this, h))
				}
			})
		} else {
			if (typeof h === "string" && h[0] !== "_" && h !== "init") {
				f = c.data(this[0], "responsivetabs");
				if (h === "destroy") {
					c.data(this, "responsivetabs", null)
				}
				if (f instanceof a && typeof f[h] === "function") {
					return f[h].apply(f, Array.prototype.slice.call(g, 1))
				} else {
					return this
				}
			}
		}
	}
}(jQuery, window));