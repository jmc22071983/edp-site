jQuery(function(a) {
	a.datepicker.regional.ar = {
		closeText : "إغلاق",
		prevText : "السابق",
		nextText : "التالي",
		currentText : "اليوم",
		monthNames : [ "كانون الثاني", "شباط", "آذار", "نيسان", "آذار",
				"حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني",
				"كانون الأول" ],
		monthNamesShort : [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
				"11", "12" ],
		dayNames : [ "السبت", "الأحد", "الاثنين", "الثلاثاء", "الأربعاء",
				"الخميس", "الجمعة" ],
		dayNamesShort : [ "سبت", "أحد", "اثنين", "ثلاثاء", "أربعاء", "خميس",
				"جمعة" ],
		dayNamesMin : [ "سبت", "أحد", "اثنين", "ثلاثاء", "أربعاء", "خميس",
				"جمعة" ],
		weekHeader : "أسبوع",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : true
	};
	a.datepicker.setDefaults(a.datepicker.regional.ar)
});
jQuery(function(a) {
	a.datepicker.regional.bg = {
		closeText : "затвори",
		prevText : "назад",
		nextText : "напред",
		currentText : "днес",
		currentStatus : "",
		monthNames : [ "Януари", "Февруари", "Март", "Април", "Май", "Юни",
				"Юли", "Август", "Септември", "Октомври", "Ноември", "Декември" ],
		monthNamesShort : [ "Яну", "Фев", "Мар", "Апр", "Май", "Юни", "Юли",
				"Авг", "Сеп", "Окт", "Нов", "Дек" ],
		dayNames : [ "Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък",
				"Петък", "Събота" ],
		dayNamesShort : [ "Нед", "Пон", "Вто", "Сря", "Чет", "Пет", "Съб" ],
		dayNamesMin : [ "Не", "По", "Вт", "Ср", "Че", "Пе", "Съ" ],
		weekHeader : "Wk",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.bg)
});
jQuery(function(a) {
	a.datepicker.regional.ca = {
		closeText : "Tancar",
		prevText : "Anterior",
		nextText : "Següent",
		currentText : "Avui",
		monthNames : [ "Gener", "Febrer", "Mar&ccedil;", "Abril", "Maig",
				"Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre",
				"Desembre" ],
		monthNamesShort : [ "Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul",
				"Ago", "Set", "Oct", "Nov", "Des" ],
		dayNames : [ "Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous",
				"Divendres", "Dissabte" ],
		dayNamesShort : [ "Dug", "Dln", "Dmt", "Dmc", "Djs", "Dvn", "Dsb" ],
		dayNamesMin : [ "Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds" ],
		weekHeader : "Sm",
		dateFormat : "mm/dd/yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.ca);
	a.timepicker.regional.ca = {
		timeOnlyTitle : "Escollir una hora",
		timeText : "Hora",
		hourText : "Hores",
		minuteText : "Minuts",
		secondText : "Segons",
		millisecText : "Milisegons",
		timezoneText : "Fus horari",
		currentText : "Ara",
		closeText : "Tancar",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.ca)
});
jQuery(function(a) {
	a.datepicker.regional.cs = {
		closeText : "Zavřít",
		prevText : "Dříve",
		nextText : "Později",
		currentText : "Nyní",
		monthNames : [ "leden", "únor", "březen", "duben", "květen", "červen",
				"červenec", "srpen", "září", "říjen", "listopad", "prosinec" ],
		monthNamesShort : [ "led", "úno", "bře", "dub", "kvě", "čer", "čvc",
				"srp", "zář", "říj", "lis", "pro" ],
		dayNames : [ "neděle", "pondělí", "úterý", "středa", "čtvrtek",
				"pátek", "sobota" ],
		dayNamesShort : [ "ne", "po", "út", "st", "čt", "pá", "so" ],
		dayNamesMin : [ "ne", "po", "út", "st", "čt", "pá", "so" ],
		weekHeader : "Týd",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.cs);
	a.timepicker.regional.cs = {
		timeOnlyTitle : "Vyberte čas",
		timeText : "Čas",
		hourText : "Hodiny",
		minuteText : "Minuty",
		secondText : "Vteřiny",
		millisecText : "Milisekundy",
		timezoneText : "Časové pásmo",
		currentText : "Nyní",
		closeText : "Zavřít",
		timeFormat : "h:m",
		amNames : [ "dop.", "AM", "A" ],
		pmNames : [ "odp.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.cs)
});
jQuery(function(a) {
	a.datepicker.regional.da = {
		closeText : "Luk",
		prevText : "Forrige",
		nextText : "Næste",
		currentText : "Idag",
		monthNames : [ "Januar", "Februar", "Marts", "April", "Maj", "Juni",
				"Juli", "August", "September", "Oktober", "November",
				"December" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul",
				"Aug", "Sep", "Okt", "Nov", "Dec" ],
		dayNames : [ "Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag",
				"Fredag", "Lørdag" ],
		dayNamesShort : [ "Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør" ],
		dayNamesMin : [ "Sø", "Ma", "Ti", "On", "To", "Fr", "Lø" ],
		weekHeader : "Uge",
		dateFormat : "dd-mm-yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.da)
});
jQuery(function(a) {
	a.datepicker.regional.de = {
		closeText : "schließen",
		prevText : "früher",
		nextText : "nächste",
		currentText : "heute",
		monthNames : [ "Januar", "Februar", "März", "April", "Mai", "Juni",
				"Juli", "August", "September", "Oktober", "November",
				"Dezember" ],
		monthNamesShort : [ "Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul",
				"Aug", "Sep", "Okt", "Nov", "Dez" ],
		dayNames : [ "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag",
				"Freitag", "Samstag" ],
		dayNamesShort : [ "So", "Mo", "Di", "Mi", "Do", "Fr", "Sa" ],
		dayNamesMin : [ "So", "Mo", "Di", "Mi", "Do", "Fr", "Sa" ],
		weekHeader : "Wo",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.de);
	a.timepicker.regional.de = {
		timeOnlyTitle : "Zeit Wählen",
		timeText : "Zeit",
		hourText : "Stunde",
		minuteText : "Minute",
		secondText : "Sekunde",
		millisecText : "Millisekunde",
		timezoneText : "Zeitzone",
		currentText : "Jetzt",
		closeText : "Fertig",
		timeFormat : "HH:mm",
		amNames : [ "vorm.", "AM", "A" ],
		pmNames : [ "nachm.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.de)
});
jQuery(function(a) {
	a.datepicker.regional.en = {
		closeText : "Close",
		prevText : "Previous",
		nextText : "Next",
		currentText : "Today",
		monthNames : [ "January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November",
				"December" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
				"Aug", "Sep", "Oct", "Nov", "Dec" ],
		dayNames : [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
				"Friday", "Saturday" ],
		dayNamesShort : [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
		dayNamesMin : [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ],
		weekHeader : "Wk",
		dateFormat : "mm/dd/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.en);
	a.timepicker.regional.en = {
		timeOnlyTitle : "Choose Time",
		timeText : "Time",
		hourText : "Hour",
		minuteText : "Minute",
		secondText : "Second",
		millisecText : "Millisecond",
		timezoneText : "Time Zone",
		currentText : "Now",
		closeText : "Done",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.en)
});
jQuery(function(a) {
	a.datepicker.regional.eo = {
		closeText : "Fermi",
		prevText : "&lt;Anta",
		nextText : "Sekv&gt;",
		currentText : "Nuna",
		monthNames : [ "Januaro", "Februaro", "Marto", "Aprilo", "Majo",
				"Junio", "Julio", "Aŭgusto", "Septembro", "Oktobro",
				"Novembro", "Decembro" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul",
				"Aŭg", "Sep", "Okt", "Nov", "Dec" ],
		dayNames : [ "Dimanĉo", "Lundo", "Mardo", "Merkredo", "Ĵaŭdo",
				"Vendredo", "Sabato" ],
		dayNamesShort : [ "Dim", "Lun", "Mar", "Mer", "Ĵaŭ", "Ven", "Sab" ],
		dayNamesMin : [ "Di", "Lu", "Ma", "Me", "Ĵa", "Ve", "Sa" ],
		weekHeader : "Sb",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.eo)
});
jQuery(function(a) {
	a.datepicker.regional.es = {
		closeText : "Cerrar",
		prevText : "Anterior",
		nextText : "Siguiente",
		currentText : "Hoy",
		monthNames : [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
				"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
				"Diciembre" ],
		monthNamesShort : [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
				"Ago", "Sep", "Oct", "Nov", "Dic" ],
		dayNames : [ "Domingo", "Lunes", "Martes", "Mi&eacute;rcoles",
				"Jueves", "Viernes", "S&aacute;bado" ],
		dayNamesShort : [ "Dom", "Lun", "Mar", "Mi&eacute;", "Juv", "Vie",
				"S&aacute;b" ],
		dayNamesMin : [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "S&aacute;" ],
		weekHeader : "Sm",
		dateFormat : "dd/mm/yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.es);
	a.timepicker.regional.es = {
		timeOnlyTitle : "Elegir una hora",
		timeText : "Hora",
		hourText : "Hora",
		minuteText : "Minuto",
		secondText : "Segundo",
		millisecText : "Milisegundo",
		timezoneText : "Huso horario",
		currentText : "Ahora",
		closeText : "Cerrar",
		timeFormat : "HH:mm",
		amNames : [ "a.m.", "AM", "A" ],
		pmNames : [ "p.m.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.es)
});
jQuery(function(a) {
	a.datepicker.regional.eu = {
		closeText : "Egina",
		prevText : "Aurreko",
		nextText : "Hurrengoa",
		currentText : "Gaur",
		monthNames : [ "urtarrila", "otsaila", "martxoa", "apirila", "maiatza",
				"ekaina", "uztaila", "abuztua", "iraila", "urria", "azaroa",
				"abendua" ],
		monthNamesShort : [ "urt.", "ots.", "mar.", "api.", "mai.", "eka.",
				"uzt.", "abu.", "ira.", "urr.", "aza.", "abe." ],
		dayNames : [ "igandea", "astelehena", "asteartea", "asteazkena",
				"osteguna", "ostirala", "larunbata" ],
		dayNamesShort : [ "ig.", "al.", "ar.", "az.", "og.", "ol.", "lr." ],
		dayNamesMin : [ "ig", "al", "ar", "az", "og", "ol", "lr" ],
		weekHeader : "As",
		dateFormat : "dd/mm/yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.eu);
	a.timepicker.regional.eu = {
		timeOnlyTitle : "Aukeratu ordua",
		timeText : "Ordua",
		hourText : "Orduak",
		minuteText : "Minutuak",
		secondText : "Segunduak",
		millisecText : "Milisegunduak",
		timezoneText : "Ordu-eremua",
		currentText : "Orain",
		closeText : "Itxi",
		timeFormat : "HH:mm",
		amNames : [ "a.m.", "AM", "A" ],
		pmNames : [ "p.m.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.eu)
});
jQuery(function(a) {
	a.datepicker.regional.fa = {
		closeText : "بستن",
		prevText : "قبلي",
		nextText : "بعدي",
		currentText : "امروز",
		monthNames : [ "فروردين", "ارديبهشت", "خرداد", "تير", "مرداد",
				"شهريور", "مهر", "آبان", "آذر", "دي", "بهمن", "اسفند" ],
		monthNamesShort : [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
				"11", "12" ],
		dayNames : [ "يکشنبه", "دوشنبه", "سهشنبه", "چهارشنبه", "پنجشنبه",
				"جمعه", "شنبه" ],
		dayNamesShort : [ "ي", "د", "س", "چ", "پ", "ج", "ش" ],
		dayNamesMin : [ "ي", "د", "س", "چ", "پ", "ج", "ش" ],
		weekHeader : "هف",
		dateFormat : "yy/mm/dd",
		firstDay : 6,
		isRTL : true
	};
	a.datepicker.setDefaults(a.datepicker.regional.fa)
});
jQuery(function(a) {
	a.datepicker.regional.fi = {
		closeText : "Sulje",
		prevText : "&laquo;Edellinen",
		nextText : "Seuraava&raquo;",
		currentText : "T&auml;n&auml;&auml;n",
		monthNames : [ "Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu",
				"Toukokuu", "Kes&auml;kuu", "Hein&auml;kuu", "Elokuu",
				"Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu" ],
		monthNamesShort : [ "Tammi", "Helmi", "Maalis", "Huhti", "Touko",
				"Kes&auml;", "Hein&auml;", "Elo", "Syys", "Loka", "Marras",
				"Joulu" ],
		dayNamesShort : [ "Su", "Ma", "Ti", "Ke", "To", "Pe", "Su" ],
		dayNames : [ "Sunnuntai", "Maanantai", "Tiistai", "Keskiviikko",
				"Torstai", "Perjantai", "Lauantai" ],
		dayNamesMin : [ "Su", "Ma", "Ti", "Ke", "To", "Pe", "La" ],
		weekHeader : "Vk",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.fi);
	a.timepicker.regional.fi = {
		timeOnlyTitle : "Valitse aika",
		timeText : "Aika",
		hourText : "Tunti",
		minuteText : "Minuutti",
		secondText : "Sekunti",
		millisecText : "Millisekunnin",
		timezoneText : "Aikavyöhyke",
		currentText : "Nyt",
		closeText : "Sulje",
		timeFormat : "HH:mm",
		amNames : [ "ap.", "AM", "A" ],
		pmNames : [ "ip.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.fi)
});
jQuery(function(a) {
	a.datepicker.regional.fr = {
		closeText : "Fermer",
		prevText : "Précédent",
		nextText : "Suivant",
		currentText : "Courant",
		monthNames : [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
				"Juillet", "Août", "Septembre", "Octobre", "Novembre",
				"Décembre" ],
		monthNamesShort : [ "Jan", "Fév", "Mar", "Avr", "Mai", "Jun", "Jul",
				"Aoû", "Sep", "Oct", "Nov", "Déc" ],
		dayNames : [ "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi",
				"Vendredi", "Samedi" ],
		dayNamesShort : [ "Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam" ],
		dayNamesMin : [ "Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa" ],
		weekHeader : "Sm",
		dateFormat : "dd/mm/yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.fr);
	a.timepicker.regional.fr = {
		timeOnlyTitle : "Choisir une heure",
		timeText : "Heure",
		hourText : "Heures",
		minuteText : "Minutes",
		secondText : "Secondes",
		millisecText : "Millisecondes",
		timezoneText : "Fuseau horaire",
		currentText : "Maintenant",
		closeText : "Terminé",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.fr)
});
jQuery(function(a) {
	a.datepicker.regional.he = {
		closeText : "סגור",
		prevText : "הקודם",
		nextText : "הבא",
		currentText : "היום",
		monthNames : [ "ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני",
				"יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר" ],
		monthNamesShort : [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
				"11", "12" ],
		dayNames : [ "ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת" ],
		dayNamesShort : [ "א'", "ב'", "ג'", "ד'", "ה'", "ו'", "שבת" ],
		dayNamesMin : [ "א'", "ב'", "ג'", "ד'", "ה'", "ו'", "שבת" ],
		weekHeader : "Sm",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : true
	};
	a.datepicker.setDefaults(a.datepicker.regional.he);
	a.timepicker.regional.he = {
		timeOnlyTitle : "בחירת זמן",
		timeText : "שעה",
		hourText : "שעות",
		minuteText : "דקות",
		secondText : "שניות",
		millisecText : "אלפית השנייה",
		timezoneText : "אזור זמן",
		currentText : "עכשיו",
		closeText : "סגור",
		timeFormat : "HH:mm",
		amNames : [ 'לפנה"צ', "AM", "A" ],
		pmNames : [ 'אחה"צ', "PM", "P" ],
		isRTL : true
	};
	a.timepicker.setDefaults(a.timepicker.regional.he)
});
jQuery(function(a) {
	a.datepicker.regional.hr = {
		closeText : "Zatvori",
		prevText : "Prethodna",
		nextText : "Sljedeća",
		currentText : "Danas",
		monthNames : [ "Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj",
				"Lipani", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni",
				"Prosinac" ],
		monthNamesShort : [ "Sij", "Velj", "Ožu", "Tra", "Svi", "Lip", "Srp",
				"Kol", "Ruj", "Lis", "Stu", "Pro" ],
		dayNames : [ "Nedjalja", "Ponedjeljak", "Utorak", "Srijeda",
				"Četvrtak", "Petak", "Subota" ],
		dayNamesShort : [ "Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub" ],
		dayNamesMin : [ "Ne", "Po", "Ut", "Sr", "Če", "Pe", "Su" ],
		weekHeader : "Tje",
		dateFormat : "dd.mm.yy.",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.hr)
});
jQuery(function(a) {
	a.datepicker.regional.hu = {
		closeText : "bezárás",
		prevText : "&laquo;&nbsp;vissza",
		nextText : "előre&nbsp;&raquo;",
		currentText : "ma",
		monthNames : [ "Január", "Február", "Március", "Április", "Május",
				"Június", "Július", "Augusztus", "Szeptember", "Október",
				"November", "December" ],
		monthNamesShort : [ "Jan", "Feb", "Már", "Ápr", "Máj", "Jún", "Júl",
				"Aug", "Szep", "Okt", "Nov", "Dec" ],
		dayNames : [ "Vasámap", "Hétfö", "Kedd", "Szerda", "Csütörtök",
				"Péntek", "Szombat" ],
		dayNamesShort : [ "Vas", "Hét", "Ked", "Sze", "Csü", "Pén", "Szo" ],
		dayNamesMin : [ "V", "H", "K", "Sze", "Cs", "P", "Szo" ],
		weekHeader : "Hé",
		dateFormat : "yy-mm-dd",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.hu);
	a.timepicker.regional.hu = {
		timeOnlyTitle : "Válasszon időpontot",
		timeText : "Idő",
		hourText : "Óra",
		minuteText : "Perc",
		secondText : "Másodperc",
		millisecText : "Milliszekundumos",
		timezoneText : "Időzóna",
		currentText : "Most",
		closeText : "Kész",
		timeFormat : "HH:mm",
		amNames : [ "de.", "AM", "A" ],
		pmNames : [ "du.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.hu)
});
jQuery(function(a) {
	a.datepicker.regional.hy = {
		closeText : "Փակել",
		prevText : "Նախ.",
		nextText : "Հաջ.",
		currentText : "Այսօր",
		monthNames : [ "Հունվար", "Փետրվար", "Մարտ", "Ապրիլ", "Մայիս",
				"Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր",
				"Նոյեմբեր", "Դեկտեմբեր" ],
		monthNamesShort : [ "Հունվ", "Փետր", "Մարտ", "Ապր", "Մայիս", "Հունիս",
				"Հուլ", "Օգս", "Սեպ", "Հոկ", "Նոյ", "Դեկ" ],
		dayNames : [ "կիրակի", "եկուշաբթի", "երեքշաբթի", "չորեքշաբթի",
				"հինգշաբթի", "ուրբաթ", "շաբաթ" ],
		dayNamesShort : [ "կիր", "երկ", "երք", "չրք", "հնգ", "ուրբ", "շբթ" ],
		dayNamesMin : [ "կիր", "երկ", "երք", "չրք", "հնգ", "ուրբ", "շբթ" ],
		weekHeader : "ՇԲՏ",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.hy)
});
jQuery(function(a) {
	a.datepicker.regional.id = {
		closeText : "Tutup",
		prevText : "mundur",
		nextText : "maju",
		currentText : "hari ini",
		monthNames : [ "Januari", "Februari", "Maret", "April", "Mei", "Juni",
				"Juli", "Agustus", "September", "Oktober", "Nopember",
				"Desember" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul",
				"Agus", "Sep", "Okt", "Nop", "Des" ],
		dayNames : [ "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat",
				"Sabtu" ],
		dayNamesShort : [ "Min", "Sen", "Sel", "Rab", "kam", "Jum", "Sab" ],
		dayNamesMin : [ "Mg", "Sn", "Sl", "Rb", "Km", "jm", "Sb" ],
		weekHeader : "Mg",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.id);
	a.timepicker.regional.id = {
		timeOnlyTitle : "Pilih Waktu",
		timeText : "Waktu",
		hourText : "Pukul",
		minuteText : "Menit",
		secondText : "Detik",
		millisecText : "Milidetik",
		timezoneText : "Zona Waktu",
		currentText : "Sekarang",
		closeText : "OK",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.id)
});
jQuery(function(a) {
	a.datepicker.regional.is = {
		closeText : "Loka",
		prevText : "Fyrri",
		nextText : "N&aelig;sti",
		currentText : "&Iacute; dag",
		monthNames : [ "Jan&uacute;ar", "Febr&uacute;ar", "Mars",
				"Apr&iacute;l", "Ma&iacute", "J&uacute;n&iacute;",
				"J&uacute;l&iacute;", "&Aacute;g&uacute;st", "September",
				"Okt&oacute;ber", "N&oacute;vember", "Desember" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Ma&iacute;",
				"J&uacute;n", "J&uacute;l", "&Aacute;g&uacute;", "Sep", "Okt",
				"N&oacute;v", "Des" ],
		dayNames : [ "Sunnudagur", "M&aacute;nudagur", "&THORN;ri&eth;judagur",
				"Mi&eth;vikudagur", "Fimmtudagur", "F&ouml;studagur",
				"Laugardagur" ],
		dayNamesShort : [ "Sun", "M&aacute;n", "&THORN;ri", "Mi&eth;", "Fim",
				"F&ouml;s", "Lau" ],
		dayNamesMin : [ "Su", "M&aacute;", "&THORN;r", "Mi", "Fi", "F&ouml;",
				"La" ],
		weekHeader : "Vika",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.is)
});
jQuery(function(a) {
	a.datepicker.regional.it = {
		closeText : "Chiudi",
		prevText : "Precedente",
		nextText : "Prossimo",
		currentText : "Oggi",
		monthNames : [ "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio",
				"Giugno", "Luglio", "Agosto", "Settembre", "Ottobre",
				"Novembre", "Dicembre" ],
		monthNamesShort : [ "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug",
				"Ago", "Set", "Ott", "Nov", "Dic" ],
		dayNames : [ "Domenica", "Luned&#236", "Marted&#236", "Mercoled&#236",
				"Gioved&#236", "Venerd&#236", "Sabato" ],
		dayNamesShort : [ "Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab" ],
		dayNamesMin : [ "Do", "Lu", "Ma", "Me", "Gio", "Ve", "Sa" ],
		weekHeader : "Sm",
		dateFormat : "dd/mm/yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.it);
	a.timepicker.regional.it = {
		timeOnlyTitle : "Scegli orario",
		timeText : "Orario",
		hourText : "Ora",
		minuteText : "Minuto",
		secondText : "Secondo",
		millisecText : "Millisecondo",
		timezoneText : "Fuso orario",
		currentText : "Adesso",
		closeText : "Chiudi",
		timeFormat : "HH:mm",
		amNames : [ "m.", "AM", "A" ],
		pmNames : [ "p.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.it)
});
jQuery(function(a) {
	a.datepicker.regional.ja = {
		closeText : "閉じる",
		prevText : "前",
		nextText : "次",
		currentText : "今日",
		monthNames : [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
				"10月", "11月", "12月" ],
		monthNamesShort : [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月",
				"9月", "10月", "11月", "12月" ],
		dayNames : [ "日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日" ],
		dayNamesShort : [ "日", "月", "火", "水", "木", "金", "土" ],
		dayNamesMin : [ "日", "月", "火", "水", "木", "金", "土" ],
		weekHeader : "週",
		dateFormat : "yy/mm/dd",
		firstDay : 0,
		isRTL : false,
		showMonthAfterYear : true
	};
	a.datepicker.setDefaults(a.datepicker.regional.ja);
	a.timepicker.regional.ja = {
		timeOnlyTitle : "時間を選択",
		timeText : "時間",
		hourText : "時",
		minuteText : "分",
		secondText : "秒",
		millisecText : "ミリ秒",
		timezoneText : "タイムゾーン",
		currentText : "現時刻",
		closeText : "閉じる",
		timeFormat : "HH:mm",
		amNames : [ "午前", "AM", "A" ],
		pmNames : [ "午後", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.ja)
});
jQuery(function(a) {
	a.datepicker.regional.ko = {
		closeText : "닫기",
		prevText : "이전달",
		nextText : "다음달",
		currentText : "오늘",
		monthNames : [ "1월(JAN)", "2월(FEB)", "3월(MAR)", "4월(APR)", "5월(MAY)",
				"6월(JUN)", "7월(JUL)", "8월(AUG)", "9월(SEP)", "10월(OCT)",
				"11월(NOV)", "12월(DEC)" ],
		monthNamesShort : [ "1월(JAN)", "2월(FEB)", "3월(MAR)", "4월(APR)",
				"5월(MAY)", "6월(JUN)", "7월(JUL)", "8월(AUG)", "9월(SEP)",
				"10월(OCT)", "11월(NOV)", "12월(DEC)" ],
		dayNames : [ "일", "월", "화", "수", "목", "금", "토" ],
		dayNamesShort : [ "일", "월", "화", "수", "목", "금", "토" ],
		dayNamesMin : [ "일", "월", "화", "수", "목", "금", "토" ],
		weekHeader : "Wk",
		dateFormat : "yy-mm-dd",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.ko);
	a.timepicker.regional.ko = {
		timeOnlyTitle : "시간 선택",
		timeText : "시간",
		hourText : "시",
		minuteText : "분",
		secondText : "초",
		millisecText : "밀리초",
		timezoneText : "표준 시간대",
		currentText : "현재 시각",
		closeText : "닫기",
		timeFormat : "tt h:mm",
		amNames : [ "오전", "AM", "A" ],
		pmNames : [ "오후", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.ko)
});
jQuery(function(a) {
	a.datepicker.regional.lt = {
		closeText : "Uždaryti",
		prevText : "Atgal",
		nextText : "Pirmyn",
		currentText : "Šiandien",
		monthNames : [ "Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė",
				"Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis",
				"Lapkritis", "Gruodis" ],
		monthNamesShort : [ "Sau", "Vas", "Kov", "Bal", "Geg", "Bir", "Lie",
				"Rugp", "Rugs", "Spa", "Lap", "Gru" ],
		dayNames : [ "sekmadienis", "pirmadienis", "antradienis",
				"trečiadienis", "ketvirtadienis", "penktadienis", "šeštadienis" ],
		dayNamesShort : [ "sek", "pir", "ant", "tre", "ket", "pen", "šeš" ],
		dayNamesMin : [ "Se", "Pr", "An", "Tr", "Ke", "Pe", "Še" ],
		weekHeader : "",
		dateFormat : "yy-mm-dd",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.lt);
	a.timepicker.regional.lt = {
		timeOnlyTitle : "Pasirinkite laiką",
		timeText : "Laikas",
		hourText : "Valandos",
		minuteText : "Minutės",
		secondText : "Sekundės",
		millisecText : "Milisekundės",
		timezoneText : "Laiko zona",
		currentText : "Dabar",
		closeText : "Uždaryti",
		timeFormat : "HH:mm",
		amNames : [ "priešpiet", "AM", "A" ],
		pmNames : [ "popiet", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.lt)
});
jQuery(function(a) {
	a.datepicker.regional.lv = {
		closeText : "Aizvērt",
		prevText : "Iepr",
		nextText : "Nāka",
		currentText : "Šodien",
		monthNames : [ "Janvāris", "Februāris", "Marts", "Aprīlis", "Maijs",
				"Jūnijs", "Jūlijs", "Augusts", "Septembris", "Oktobris",
				"Novembris", "Decembris" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Mai", "Jūn", "Jūl",
				"Aug", "Sep", "Okt", "Nov", "Dec" ],
		dayNames : [ "svētdiena", "pirmdiena", "otrdiena", "trešdiena",
				"ceturtdiena", "piektdiena", "sestdiena" ],
		dayNamesShort : [ "svt", "prm", "otr", "tre", "ctr", "pkt", "sst" ],
		dayNamesMin : [ "Sv", "Pr", "Ot", "Tr", "Ct", "Pk", "Ss" ],
		weekHeader : "Nav",
		dateFormat : "dd-mm-yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.lv)
});
jQuery(function(a) {
	a.datepicker.regional.nl = {
		closeText : "Sluiten",
		prevText : "←",
		nextText : "→",
		currentText : "Vandaag",
		monthNames : [ "januari", "februari", "maart", "april", "mei", "juni",
				"juli", "augustus", "september", "oktober", "november",
				"december" ],
		monthNamesShort : [ "jan", "feb", "maa", "apr", "mei", "jun", "jul",
				"aug", "sep", "okt", "nov", "dec" ],
		dayNames : [ "zondag", "maandag", "dinsdag", "woensdag", "donderdag",
				"vrijdag", "zaterdag" ],
		dayNamesShort : [ "zon", "maa", "din", "woe", "don", "vri", "zat" ],
		dayNamesMin : [ "zo", "ma", "di", "wo", "do", "vr", "za" ],
		weekHeader : "Wk",
		dateFormat : "dd/mm/yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.nl);
	a.timepicker.regional.nl = {
		timeOnlyTitle : "Tijdstip",
		timeText : "Tijd",
		hourText : "Uur",
		minuteText : "Minuut",
		secondText : "Seconde",
		millisecText : "Milliseconde",
		timezoneText : "Tijdzone",
		currentText : "Vandaag",
		closeText : "Sluiten",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.nl)
});
jQuery(function(a) {
	a.datepicker.regional.no = {
		closeText : "Lukk",
		prevText : "&laquo;Forrige",
		nextText : "Neste&raquo;",
		currentText : "I dag",
		monthNames : [ "Januar", "Februar", "Mars", "April", "Mai", "Juni",
				"Juli", "August", "September", "Oktober", "November",
				"Desember" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul",
				"Aug", "Sep", "Okt", "Nov", "Des" ],
		dayNamesShort : [ "Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør" ],
		dayNames : [ "Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag",
				"Fredag", "Lørdag" ],
		dayNamesMin : [ "Sø", "Ma", "Ti", "On", "To", "Fr", "Lø" ],
		weekHeader : "Uke",
		dateFormat : "yy-mm-dd",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.no);
	a.timepicker.regional.no = {
		timeOnlyTitle : "Velg tid",
		timeText : "Tid",
		hourText : "Time",
		minuteText : "Minutt",
		secondText : "Sekund",
		millisecText : "Millisekund",
		timezoneText : "Tidssone",
		currentText : "Nå",
		closeText : "Lukk",
		timeFormat : "HH:mm",
		amNames : [ "am", "AM", "A" ],
		pmNames : [ "pm", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.no)
});
jQuery(function(a) {
	a.datepicker.regional.pl = {
		closeText : "Zamknij",
		prevText : "Poprzedni",
		nextText : "Następny",
		currentText : "Dziś",
		monthNames : [ "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj",
				"Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik",
				"Listopad", "Grudzień" ],
		monthNamesShort : [ "Sty", "Lu", "Mar", "Kw", "Maj", "Cze", "Lip",
				"Sie", "Wrz", "Pa", "Lis", "Gru" ],
		dayNames : [ "Niedziela", "Poniedzialek", "Wtorek", "Środa",
				"Czwartek", "Piątek", "Sobota" ],
		dayNamesShort : [ "Nie", "Pn", "Wt", "Śr", "Czw", "Pt", "So" ],
		dayNamesMin : [ "N", "Pn", "Wt", "Śr", "Cz", "Pt", "So" ],
		weekHeader : "Tydz",
		dateFormat : "yy-mm-dd",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.pl);
	a.timepicker.regional.pl = {
		timeOnlyTitle : "Wybierz godzinę",
		timeText : "Czas",
		hourText : "Godzina",
		minuteText : "Minuta",
		secondText : "Sekunda",
		millisecText : "Milisekunda",
		timezoneText : "Strefa czasowa",
		currentText : "Teraz",
		closeText : "Gotowe",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.pl)
});
jQuery(function(a) {
	a.datepicker.regional["pt-BR"] = {
		closeText : "Fechar",
		prevText : "Anterior",
		nextText : "Pr&oacute;ximo",
		currentText : "Hoje",
		monthNames : [ "Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio",
				"Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro",
				"Dezembro" ],
		monthNamesShort : [ "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul",
				"Ago", "Set", "Out", "Nov", "Dez" ],
		dayNames : [ "Domingo", "Segunda-feira", "Ter&ccedil;a-feira",
				"Quarta-feira", "Quinta-feira", "Sexta-feira", "Sabado" ],
		dayNamesShort : [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab" ],
		dayNamesMin : [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab" ],
		weekHeader : "Sm",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional["pt-BR"]);
	a.timepicker.regional["pt-BR"] = {
		timeOnlyTitle : "Escolha a hora",
		timeText : "Hora",
		hourText : "Horas",
		minuteText : "Minutos",
		secondText : "Segundos",
		millisecText : "Milissegundos",
		timezoneText : "Fuso horário",
		currentText : "Agora",
		closeText : "Fechar",
		timeFormat : "HH:mm",
		amNames : [ "a.m.", "AM", "A" ],
		pmNames : [ "p.m.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional["pt-BR"])
});
jQuery(function(a) {
	a.datepicker.regional.pt = {
		closeText : "Fechar",
		prevText : "Anterior",
		nextText : "Pr&oacute;ximo",
		currentText : "Hoje",
		monthNames : [ "Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio",
				"Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro",
				"Dezembro" ],
		monthNamesShort : [ "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul",
				"Ago", "Set", "Out", "Nov", "Dez" ],
		dayNames : [ "Domingo", "Segunda-feira", "Ter&ccedil;a-feira",
				"Quarta-feira", "Quinta-feira", "Sexta-feira", "Sabado" ],
		dayNamesShort : [ "Do", "Se", "Te", "Qua", "Qui", "Sx", "Sa" ],
		dayNamesMin : [ "Do", "Se", "Te", "Qua", "Qui", "Sx", "Sa" ],
		weekHeader : "Sm",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.pt);
	a.timepicker.regional.pt = {
		timeOnlyTitle : "Escolha a hora",
		timeText : "Hora",
		hourText : "Horas",
		minuteText : "Minutos",
		secondText : "Segundos",
		millisecText : "Milissegundos",
		timezoneText : "Fuso horário",
		currentText : "Agora",
		closeText : "Fechar",
		timeFormat : "HH:mm",
		amNames : [ "a.m.", "AM", "A" ],
		pmNames : [ "p.m.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.pt)
});
jQuery(function(a) {
	a.datepicker.regional.ro = {
		closeText : "Inchide",
		prevText : "Inapoi",
		nextText : "Urmator",
		currentText : "Azi",
		monthNames : [ "Ianuarie", "Februarie", "Martie", "Aprilie", "Mai",
				"Junie", "Julie", "August", "Septembrie", "Octobrie",
				"Noiembrie", "Decembrie" ],
		monthNamesShort : [ "Ian", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul",
				"Aug", "Sep", "Oct", "Noi", "Dec" ],
		dayNames : [ "Duminica", "Luni", "Marti", "Miercuri", "Joi", "Vineri",
				"Sambata" ],
		dayNamesShort : [ "Dum", "Lun", "Mar", "Mie", "Joi", "Vin", "Sam" ],
		dayNamesMin : [ "Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sa" ],
		weekHeader : "Sapt",
		dateFormat : "mm/dd/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.ro);
	a.timepicker.regional.ro = {
		timeOnlyTitle : "Alegeţi o oră",
		timeText : "Timp",
		hourText : "Ore",
		minuteText : "Minute",
		secondText : "Secunde",
		millisecText : "Milisecunde",
		timezoneText : "Fus orar",
		currentText : "Acum",
		closeText : "Închide",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.ro)
});
jQuery(function(a) {
	a.datepicker.regional.ru = {
		closeText : "Закрыть",
		prevText : "Пред",
		nextText : "След",
		currentText : "Сегодня",
		monthNames : [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
				"Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
		monthNamesShort : [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл",
				"Авг", "Сен", "Окт", "Ноя", "Дек" ],
		dayNames : [ "воскресенье", "понедельник", "вторник", "среда",
				"четверг", "пятница", "суббота" ],
		dayNamesShort : [ "вск", "пнд", "втр", "срд", "чтв", "птн", "сбт" ],
		dayNamesMin : [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
		weekHeader : "Не",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.ru);
	a.timepicker.regional.ru = {
		timeOnlyTitle : "Выберите время",
		timeText : "Время",
		hourText : "Часы",
		minuteText : "Минуты",
		secondText : "Секунды",
		millisecText : "Миллисекунды",
		timezoneText : "Часовой пояс",
		currentText : "Сейчас",
		closeText : "Закрыть",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.ru)
});
jQuery(function(a) {
	a.datepicker.regional.sk = {
		closeText : "Zavrieť",
		prevText : "Predchádzajúci",
		nextText : "Nasledujúci",
		currentText : "Dnes",
		monthNames : [ "Január", "Február", "Marec", "Apríl", "Máj", "Jún",
				"Júl", "August", "September", "Október", "November", "December" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Máj", "Jún", "Júl",
				"Aug", "Sep", "Okt", "Nov", "Dec" ],
		dayNames : [ "Nedel'a", "Pondelok", "Utorok", "Streda", "Štvrtok",
				"Piatok", "Sobota" ],
		dayNamesShort : [ "Ned", "Pon", "Uto", "Str", "Štv", "Pia", "Sob" ],
		dayNamesMin : [ "Ne", "Po", "Ut", "St", "Št", "Pia", "So" ],
		weekHeader : "Ty",
		dateFormat : "dd.mm.yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.sk);
	a.timepicker.regional.sk = {
		timeOnlyTitle : "Zvoľte čas",
		timeText : "Čas",
		hourText : "Hodiny",
		minuteText : "Minúty",
		secondText : "Sekundy",
		millisecText : "Milisekundy",
		timezoneText : "Časové pásmo",
		currentText : "Teraz",
		closeText : "Zavrieť",
		timeFormat : "h:m",
		amNames : [ "dop.", "AM", "A" ],
		pmNames : [ "pop.", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.sk)
});
jQuery(function(a) {
	a.datepicker.regional.sl = {
		closeText : "Zapri",
		prevText : "&lt;Prej&#x161;nji",
		nextText : "Naslednji&gt;",
		currentText : "Trenutni",
		monthNames : [ "Januar", "Februar", "Marec", "April", "Maj", "Junij",
				"Julij", "Avgust", "September", "Oktober", "November",
				"December" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul",
				"Avg", "Sep", "Okt", "Nov", "Dec" ],
		dayNames : [ "Nedelja", "Ponedeljek", "Torek", "Sreda",
				"&#x10C;etrtek", "Petek", "Sobota" ],
		dayNamesShort : [ "Ned", "Pon", "Tor", "Sre", "&#x10C;et", "Pet", "Sob" ],
		dayNamesMin : [ "Ne", "Po", "To", "Sr", "&#x10C;e", "Pe", "So" ],
		weekHeader : "Teden",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.sl)
});
jQuery(function(a) {
	a.datepicker.regional.sq = {
		closeText : "mbylle",
		prevText : "mbrapa",
		nextText : "Përpara",
		currentText : "sot",
		monthNames : [ "Janar", "Shkurt", "Mars", "Pril", "Maj", "Qershor",
				"Korrik", "Gusht", "Shtator", "Tetor", "Nëntor", "Dhjetor" ],
		monthNamesShort : [ "Jan", "Shk", "Mar", "Pri", "Maj", "Qer", "Kor",
				"Gus", "Sht", "Tet", "Nën", "Dhj" ],
		dayNames : [ "E Diel", "E Hënë", "E Martë", "E Mërkurë", "E Enjte",
				"E Premte", "E Shtune" ],
		dayNamesShort : [ "Di", "Hë", "Ma", "Më", "En", "Pr", "Sh" ],
		dayNamesMin : [ "Di", "Hë", "Ma", "Më", "En", "Pr", "Sh" ],
		weekHeader : "Ja",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.sq)
});
jQuery(function(a) {
	a.datepicker.regional.sv = {
		closeText : "Stäng",
		prevText : "&laquo;Förra",
		nextText : "Nästa&raquo;",
		currentText : "Idag",
		monthNames : [ "Januari", "Februari", "Mars", "April", "Maj", "Juni",
				"Juli", "Augusti", "September", "Oktober", "November",
				"December" ],
		monthNamesShort : [ "Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul",
				"Aug", "Sep", "Okt", "Nov", "Dec" ],
		dayNamesShort : [ "Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör" ],
		dayNames : [ "Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag",
				"Fredag", "Lördag" ],
		dayNamesMin : [ "Sö", "Må", "Ti", "On", "To", "Fr", "Lö" ],
		weekHeader : "Ve",
		dateFormat : "yy-mm-dd",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.sv);
	a.timepicker.regional.sv = {
		timeOnlyTitle : "Välj en tid",
		timeText : "Timme",
		hourText : "Timmar",
		minuteText : "Minuter",
		secondText : "Sekunder",
		millisecText : "Millisekunder",
		timezoneText : "Tidszon",
		currentText : "Nu",
		closeText : "Stäng",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.sv)
});
jQuery(function(a) {
	a.datepicker.regional.th = {
		closeText : "ปิด",
		prevText : "&laquo;&nbsp;ย้อน",
		nextText : "ถัดไป&nbsp;&raquo;",
		currentText : "วันนี้",
		monthNames : [ "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม",
				"มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม",
				"พฤศจิกายน", "ธันวาคม" ],
		monthNamesShort : [ "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.",
				"ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค." ],
		dayNames : [ "อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์",
				"เสาร์" ],
		dayNamesShort : [ "อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส." ],
		dayNamesMin : [ "อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส." ],
		weekHeader : "Sm",
		dateFormat : "dd/mm/yy",
		firstDay : 0,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.th)
});
jQuery(function(a) {
	a.datepicker.regional.tr = {
		closeText : "kapat",
		prevText : "geri",
		nextText : "ileri&#x3e",
		currentText : "bugün",
		monthNames : [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran",
				"Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
		monthNamesShort : [ "Oca", "Şub", "Mar", "Nis", "May", "Haz", "Tem",
				"Ağu", "Eyl", "Eki", "Kas", "Ara" ],
		dayNames : [ "Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe",
				"Cuma", "Cumartesi" ],
		dayNamesShort : [ "Pz", "Pt", "Sa", "Ça", "Pe", "Cu", "Ct" ],
		dayNamesMin : [ "Pz", "Pt", "Sa", "Ça", "Pe", "Cu", "Ct" ],
		weekHeader : "Hf",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.tr);
	a.timepicker.regional.tr = {
		timeOnlyTitle : "Zaman Seçiniz",
		timeText : "Zaman",
		hourText : "Saat",
		minuteText : "Dakika",
		secondText : "Saniye",
		millisecText : "Milisaniye",
		timezoneText : "Zaman Dilimi",
		currentText : "Şu an",
		closeText : "Tamam",
		timeFormat : "HH:mm",
		amNames : [ "ÖÖ", "Ö" ],
		pmNames : [ "ÖS", "S" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional.tr)
});
jQuery(function(a) {
	a.datepicker.regional.uk = {
		closeText : "Закрити",
		prevText : "Попередня",
		nextText : "Наступна",
		currentText : "Сьогодні",
		monthNames : [ "Січень", "Лютий", "Березень", "Квітень", "Травень",
				"Червень", "Липень", "Серпень", "Вересень", "Жовтень",
				"Листопад", "Грудень" ],
		monthNamesShort : [ "Січ", "Лют", "Бер", "Кві", "Тра", "Чер", "Лип",
				"Сер", "Вер", "Жов", "Лис", "Гру" ],
		dayNames : [ "неділя", "понеділок", "вівторок", "середа", "четвер",
				"пятниця", "суббота" ],
		dayNamesShort : [ "нед", "пнд", "вів", "срд", "чтв", "птн", "сбт" ],
		dayNamesMin : [ "Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
		weekHeader : "Не",
		dateFormat : "dd.mm.yy",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional.uk)
});
jQuery(function(a) {
	a.datepicker.regional["zh-CN"] = {
		closeText : "关闭",
		prevText : "上月",
		nextText : "下月",
		currentText : "今天",
		monthNames : [ "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月",
				"十月", "十一月", "十二月" ],
		monthNamesShort : [ "一", "二", "三", "四", "五", "六", "七", "八", "九", "十",
				"十一", "十二" ],
		dayNames : [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ],
		dayNamesShort : [ "周日", "周一", "周二", "周三", "周四", "周五", "周六" ],
		dayNamesMin : [ "日", "一", "二", "三", "四", "五", "六" ],
		weekHeader : "周",
		dateFormat : "yy-mm-dd",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional["zh-CN"]);
	a.timepicker.regional["zh-CN"] = {
		timeOnlyTitle : "选择时间",
		timeText : "时间",
		hourText : "小时",
		minuteText : "分钟",
		secondText : "秒钟",
		millisecText : "微秒",
		timezoneText : "时区",
		currentText : "现在时间",
		closeText : "关闭",
		timeFormat : "HH:mm",
		amNames : [ "AM", "A" ],
		pmNames : [ "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional["zh-CN"])
});
jQuery(function(a) {
	a.datepicker.regional["zh-TW"] = {
		closeText : "關閉",
		prevText : "上月",
		nextText : "下月",
		currentText : "今天",
		monthNames : [ "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月",
				"十月", "十一月", "十二月" ],
		monthNamesShort : [ "一", "二", "三", "四", "五", "六", "七", "八", "九", "十",
				"十一", "十二" ],
		dayNames : [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ],
		dayNamesShort : [ "周日", "周一", "周二", "周三", "周四", "周五", "周六" ],
		dayNamesMin : [ "日", "一", "二", "三", "四", "五", "六" ],
		weekHeader : "周",
		dateFormat : "yy/mm/dd",
		firstDay : 1,
		isRTL : false
	};
	a.datepicker.setDefaults(a.datepicker.regional["zh-TW"]);
	a.timepicker.regional["zh-TW"] = {
		timeOnlyTitle : "選擇時分秒",
		timeText : "時間",
		hourText : "時",
		minuteText : "分",
		secondText : "秒",
		millisecText : "毫秒",
		timezoneText : "時區",
		currentText : "現在時間",
		closeText : "確定",
		timeFormat : "HH:mm",
		amNames : [ "上午", "AM", "A" ],
		pmNames : [ "下午", "PM", "P" ],
		isRTL : false
	};
	a.timepicker.setDefaults(a.timepicker.regional["zh-TW"])
});