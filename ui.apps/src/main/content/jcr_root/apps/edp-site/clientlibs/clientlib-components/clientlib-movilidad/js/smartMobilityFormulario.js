var URL_OBTENER_CAP = "/SmartMobilityAjaxController/obtenerCAP.ajax";
var URL_OBTENER_PROVINCIAS = "/SmartMobilityAjaxController/obtenerProvincias.ajax";
var URL_OBTENER_PRESUPUESTO = "/SmartMobilityAjaxController/obtenerForm.ajax";
var URL_OBTENER_GUARDAR_PREGUNTAS = "/SmartMobilityAjaxController/guardarPreguntas.ajax";

var paso = '1';
var caminoAnterior = 0;
var cpValido = false;
var arrow_grados = 90;

/*global vars for show next steps*/
var caminoActualSelected = "";
var presupuestoAMedidaSelected = "";
var formularioEnviado=false;

$(document).ready(function(){
	mostrarCaminoConDatos();
	comprobarNavegador();
	comprobarModalSMS();
    bordesEliges();
    bordesRecarga();
    listenerSelectDistancia();
    precioDesplegable();
   //cargaInicialPreguntas();
    if($('#esCliente').val() === 'S'){
    	listenerClientes();
	} else{
		listenerBotonEstimacion();
        listenerBotonSinEstimacion();
	}
    presupuestoAMedida();
    listenerSelectPlantaContador();
    listenerSelectPlantaGaraje();
    listenerBtnVolverAIntentarAjax();
    listenerContinueButtons();
    crearListenerCiudad('smartMobility-provincia', 'listado-provincias');
    loadWhenItsFullyReady();
    listenerModalReSend();
    listenerSendButton();
    $("#smartMobility-codigoPostal").focusout(function(){
    	cpValido = false;
    	validarCodigoPostal($('#smartMobility-codigoPostal').val());
    	if(cpValido){
    		ajaxObtenerDatosCiudad(obtenerDatosCp($('#smartMobility-codigoPostal').val()),'listado-provincias');
    	}else{
    		$('#smartMobility-provincia').val("");
    	}
	});
	 $("#listado-provincias").on( "click","li a" , function() {
		 clickCiudad(this, 'listado-provincias');
	});
	
	if($('#error').val() == "1" || $('#modalCorrecto').val() == "1"){
		//comprobar errores formulario
	 comprobarEnvioFormulario();
	}else{
	 //Etiquetado inicial
	 etiquetadoGTMInicial();
	 //Etiquetado
	 etiquetadoGTM();
	 //comprobar errores formulario
	 comprobarEnvioFormulario();
	}
});

/*
 *	Google Tag Manager
 */
function etiquetadoGTMInicial(){
}

function etiquetadoGTM(){
	var tipoGaraje;
	var distanciaPlazaContador;
	var puntoRecarga;
	var presupuestoMedida;

	$("#btn-preg1").on( "click", function(){
		if($('#inuptTipoVivienda').val() === '1A'){
			tipoGaraje = 'vivienda-unifamiliar';
		}else if($('#inuptTipoVivienda').val() === '1B'){
			tipoGaraje = 'garaje-comunitario-misma-vivienda';
		}else if($('#inuptTipoVivienda').val() === '1C'){
			tipoGaraje = 'garaje-comunitario-distinta-vivienda';
		}else if($('#inuptTipoVivienda').val() === '1D'){
			tipoGaraje = 'ninguna';
		}

		if(tipoGaraje != null && tipoGaraje != undefined  && tipoGaraje != ''){
			dataLayerConfirmPush(tipoGaraje, '1');
		}
	});

	$("#btn-preg-dist").on( "click", function(){
		if($('#distanciaContadorGaraje').val() === '1'){
			distanciaPlazaContador = '0-10';
		}
		if($('#distanciaContadorGaraje').val() === '2'){
			distanciaPlazaContador = '10-20';
		}else if($('#distanciaContadorGaraje').val() === '3'){
			distanciaPlazaContador = '20-30';
		}else if($('#distanciaContadorGaraje').val() === '4'){
			distanciaPlazaContador = '30-40';
		}else if($('#distanciaContadorGaraje').val() === '5'){
			distanciaPlazaContador = '40-50';
		}else if($('#distanciaContadorGaraje').val() === '6'){
			distanciaPlazaContador = '50-60';
		}else if($('#distanciaContadorGaraje').val() === '7'){
			distanciaPlazaContador = '60-70';
		}else if($('#distanciaContadorGaraje').val() === '8'){
			distanciaPlazaContador = '70-80';
		}else if($('#distanciaContadorGaraje').val() === '9'){
			distanciaPlazaContador = '80-90';
		}else if($('#distanciaContadorGaraje').val() === '10'){
			distanciaPlazaContador = '90-100';
		}else if($('#distanciaContadorGaraje').val() === '11'){
			distanciaPlazaContador = '100-110';
		}else if($('#distanciaContadorGaraje').val() === '12'){
			distanciaPlazaContador = '110-120';
		}else if($('#distanciaContadorGaraje').val() === '13'){
			distanciaPlazaContador = '120-130';
		}else if($('#distanciaContadorGaraje').val() === '14'){
			distanciaPlazaContador = '130-140';
		}else if($('#distanciaContadorGaraje').val() === '15'){
			distanciaPlazaContador = '140-150';
		}

		if(distanciaPlazaContador != null && distanciaPlazaContador != undefined  && distanciaPlazaContador != ''){
			dataLayerConfirmPush(distanciaPlazaContador, '2');
		}
	});

	$("#btn-preg-recarga").on( "click", function(){
		puntoRecarga = $('#inputPuntoRecarga').val();
		if(puntoRecarga=="Home"){
			puntoRecarga="home";
		}else if (puntoRecarga=="Pulsar"){
			puntoRecarga="pulsar";
		}else{
			puntoRecarga="commander";
		}
		if(puntoRecarga != null && puntoRecarga != undefined  && puntoRecarga != ''){
			dataLayerConfirmPush(puntoRecarga, '3');
			dataLayerConfirmPushParametros(puntoRecarga, '4');
		}
	});

	$("#btn-preg5").on( "click", function(){
		presupuestoMedida = $('#inputPresupuestoMedida').val();
		if(presupuestoMedida != null && presupuestoMedida != undefined  && presupuestoMedida != ''){
			dataLayerConfirmPush(presupuestoMedida, '4');
			if(presupuestoMedida == "si"){
				puntoRecarga = $('#inputPuntoRecarga').val();
			if(puntoRecarga=="Home"){
				puntoRecarga="home";
			}else if (puntoRecarga=="Pulsar"){
				puntoRecarga="pulsar";
			}else{
				puntoRecarga="commander";
			}
				dataLayerConfirmPushParametros(puntoRecarga, '5');
			}
		}
	});
}
function comprobarEnvioFormulario(){
	if($('#error').val() == "1"){
			//Si hay errores en el formulario, se env�a este evento
			error=obtenerErrores();
			dataLayerConfirmPushFormKO("Ha ocurrido un error en los campos "+ error);
	}else if ($('#modalCorrecto').val() == "1"){
			//Si no hay errores en el formulario, se lanzan los dos eventos que nos piden
			puntoRecarga = $('#inputPuntoRecarga').val();
			if(puntoRecarga=="Home"){
				puntoRecarga="home";
			}else if (puntoRecarga=="Pulsar"){
				puntoRecarga="pulsar";
			}else{
				puntoRecarga="commander";
			}
			dataLayerConfirmPushParametros(puntoRecarga, '6');
			dataLayerConfirmPushFormOK();
	}
}
function obtenerErrores(){
	var mensaje=""; 
	$('.help-block').each(function(){ 
	mensaje=mensaje+" "+($(this).prev().attr('placeholder'));})

return mensaje;
}


function dataLayerConfirmPush(opcionSeleccionada, step) {
	if (opcionSeleccionada=="ninguna")
	{
		dataLayer.push({
		'event': 'ecommerce.js',
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'ecommerce': {
		'currencyCode': 'EUR',
		'checkout': {
		'actionField': {'step': "2"},
		'products': [
		{ 'id': 'smart-mobility',
		'name': 'smart-mobility',
		'category': 'hogares/soluciones-smart/smart-mobility',
		'price': '1',
		'quantity': '1'
		}
		]
		}
		}
		});
		dataLayer.push({
		'event': 'ecommerce.js',
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'ecommerce': {
		'checkout_option': {
		'actionField': {
		'step': "2",
		'option': 'ninguna'}
		}
		}
		});
		dataLayer.push({
		'event': 'ecommerce.js',
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'ecommerce': {
		'currencyCode': 'EUR',
		'checkout': {
		'actionField': {'step': "3"},
		'products': [
		{ 'id': 'smart-mobility',
		'name': 'smart-mobility',
		'category': 'hogares/soluciones-smart/smart-mobility',
		'price': '1',
		'quantity': '1'
		}
		]
		}
		}
		});
		dataLayer.push({
		'event': 'ecommerce.js',
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'ecommerce': {
		'checkout_option': {
		'actionField': {
		'step': "3",
		'option': "ninguna"}
		}
		}
		});
				
		dataLayerConfirmPushParametros("ninguna", '4');
	}
else{
	dataLayer.push({
		'event': 'ecommerce.js',
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'ecommerce': {
		'checkout_option': {
		'actionField': {
		'step': step,
		'option': opcionSeleccionada}
		}
		}
		});
}
}

function dataLayerConfirmPushParametros(opcionSeleccionada, step) {
	dataLayer.push({
		'event': 'ecommerce.js',
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'ecommerce': {
		'currencyCode': 'EUR',
		'checkout': {
		'actionField': {'step': step},
		'products': [
		{ 'id': 'smart-mobility',
		'name': 'smart-mobility',
		'category': 'hogares/soluciones-smart/smart-mobility',
		'price': '1',
		'quantity': '1',
		'variant': opcionSeleccionada
		}
		]
		}
		}
		});
}

function dataLayerConfirmPushFormOK() {
	dataLayer.push({
		'eventCategory': 'ok',
		'eventAction': 'funnels-venta-6�solicita-presupuesto',
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'event': 'eventAnalytics'
		});
}

function dataLayerConfirmPushFormKO(error) {
	dataLayer.push({
		'eventCategory': 'error',
		'eventAction': 'funnels-venta-6�solicita-presupuesto',
		'eventLabel': error,
		'nombre_proceso': 'solicitar-presupuesto-punto-recarga',
		'event': 'eventAnalytics'
		});
}

function comprobarNavegador(){
	if((navigator.userAgent.indexOf("MSIE") !== '-1' ) || document.documentMode)
    {
      $('.form-smart-solar').css('display','inline-table');
    }
}

function loadWhenItsFullyReady(){
	var readyStateCheckInterval = setInterval(function(){
		if (document.readyState === "complete"){
			mostrarCaminoVuelta();
			marcarErroresConAncla();
			clearInterval(readyStateCheckInterval);
		}
	},500);
}

function onSubmit() {
	$('#form-smart-mobility-datosPers').submit();
}

function comprobarModalSMS() {
	if($('#modalSms').val() === '1'){
		$('#modal-smscode').modal('show');
	}
	else if($('#modalCorrecto').val() === '1'){
		$('#modal-confirm').modal('show');
	}
	else if($('#modalError').val() === '1'){
		$('#modal-problem').modal('show');
	}
}


function bordesEliges(){
    for (var i = 1; i < 5; i++) {
        var elige = ".elige"+i;
        $(elige).click(function(){loopBordesEliges($(this));});
    }
}

function loopBordesEliges(att){
    //quitar la clase selected remove class
    $(".list-unstyled .elige span").removeClass("selected");
    //poner el selected a la clase que lo a clicado
    $(att).addClass("selected");
    $('.contenedor-smartMobility-elige').attr('data-selected', $(att).attr('id'));
    $('#inuptTipoVivienda').val($(att).attr('data-value'));
	if(paso!==$(att).attr('data-block')){
		var caminoActual = $(att).attr("class").replace("selected","").trim();
		if(opcionCamino(caminoActual)){
			eliminarSiguientes($(att).attr('data-block'));
		}
		caminoAnterior = caminoActual;
	}
	caminoActualSelected = $(att).attr('id');
	//scrollHastaAncla("btn-preg1",500,200);
}

function opcionCamino(caminoActual) {
	if(caminoActual !== caminoAnterior &&  !(caminoAnterior === 'elige2' && caminoActual === 'elige3') && opcionCaminoAux(caminoActual)){
		return true;
	}
	return false;
}

function opcionCaminoAux(caminoActual) {
	if(!(caminoAnterior === 'elige3' && caminoActual === 'elige2')){
		return true;
	}
	return false;
}

function listenerContinueButtons(){
	$("#btn-preg1").on("click",function(e){
		e.preventDefault();
		if($("#preg1 ul li span#preg1d").hasClass("selected") && $('#esCliente').val() === 'N'){
			ajaxEnviarPreguntasYRespuestas(recogerValores());
			seleccionarPreguntaDistancia(caminoActualSelected);
		}
		else if($("#preg1 ul li span").hasClass("selected")){
			seleccionarPreguntaDistancia(caminoActualSelected);
		}
	});
	$("#btn-preg-dist").on("click",function(e){
		e.preventDefault();
		if($("#distanciaContadorGaraje").val() > 0){
			var scrollParams = {element:'recarga',top:500,speed:200};
			showNextStep($(".contenedor-smartMobility-recarga"), scrollParams);
		}
	});
	$("#btn-preg3").on("click",function(e){
		e.preventDefault();
		if($("#plantaContador").val() > 0){
			var scrollParams = {element:'preg4',top:500,speed:200};
			showNextStep($(".contenedor-smartMobility-planta-garaje"), scrollParams);
		}
	});
	$("#btn-preg4").on("click",function(e){
		e.preventDefault();
		if($("#plantaGaraje").val() > 0){
			seleccionarPreguntaSelDistancia($('#preg1').attr('data-selected'));
	        //scrollHastaAncla("sel-distancia",500,200);
		}
	});
	$("#btn-preg-recarga").on("click",function(e){
		e.preventDefault();
		if($("#recarga ul li span").hasClass("selected")){
			ajaxPedirPresupuesto(recogerValores());
		}
		setClassPrecioIncluye($('#preg1').attr('data-selected'));
		if($('.precio-oculto').attr('style') === 'display: block;') {
			$('.imagen-oculta').css('-webkit-transform','rotate(0deg)');
		    $('.imagen-oculta').css('-moz-transform','rotate(0deg)');
		    $('.imagen-oculta').css('transform','rotate(0deg)');
		}
	});
	$("#btn-preg5").on("click",function(e){
		e.preventDefault();
		if($("#preg5 ul li span").hasClass("selected")){
			if(presupuestoAMedidaSelected === "No"){
				window.location.href = context + '/iberdrola/servicios/vehiculo-electrico/punto-recarga';
			}else{
				$("#solicita").fadeIn("slow", null);
				//scrollHastaAncla("solicita", 500, 200);
			}
		}

		ajaxEnviarPreguntasYRespuestas(recogerValores());
	});
}

function setClassPrecioIncluye(caminoEscogido) {
	switch(caminoEscogido) {
	case 'preg1a':
		if ($('#precio-oculto').attr('class') === ""){
			$('#precio-oculto').addClass("precio-oculto-1a row");
		} else if(($('#precio-oculto').attr('class').includes("precio-oculto-1b"))){
			$('#precio-oculto').removeClass("precio-oculto-1b");
			$('#precio-oculto').addClass("precio-oculto-1a");
		} else {
			$('#precio-oculto').removeClass("precio-oculto-1c");
			$('#precio-oculto').addClass("precio-oculto-1a");
		}
		break;
	case 'preg1b':
		if ($('#precio-oculto').attr('class') === ""){
			$('#precio-oculto').addClass("precio-oculto-1b row");
		} else if (($('#precio-oculto').attr('class').includes("precio-oculto-1a"))){
			$('#precio-oculto').removeClass("precio-oculto-1a");
			$('#precio-oculto').addClass("precio-oculto-1b");
		} else {
			$('#precio-oculto').removeClass("precio-oculto-1c");
			$('#precio-oculto').addClass("precio-oculto-1b");
		}
		break;
	case 'preg1c':
		if ($('#precio-oculto').attr('class') === ""){
			$('#precio-oculto').addClass("precio-oculto-1c row");
		} else if (($('#precio-oculto').attr('class').includes("precio-oculto-1a"))){
			$('#precio-oculto').removeClass("precio-oculto-1c");
			$('#precio-oculto').addClass("precio-oculto-1a");
		} else {
			$('#precio-oculto').removeClass("precio-oculto-1b");
			$('#precio-oculto').addClass("precio-oculto-1c");
		}
		break;
	default:
		break;
	}
}


function seleccionarPreguntaDistancia(id) {
	switch(id) {
    case "preg1a":
    	paso='4';
    	$('#preg2a').show();
    	$('#preg2b').hide();
    	$('#preg1').attr('data-selected','preg1a');
        $(".contenedor-smartMobility-distancia").fadeIn('slow', null);
       // scrollHastaAncla("preg2a",500,200);
    	break;
    case "preg1b":
    	paso='2';
    	$('#preg1').attr('data-selected','preg1b');
      $(".contenedor-smartMobility-planta-contador").fadeIn('slow', null);
      //scrollHastaAncla("btn-preg3",500,200);
    	break;
    case "preg1c":
    	paso='2';
    	$('#preg1').attr('data-selected','preg1c');
        $(".contenedor-smartMobility-planta-contador").fadeIn('slow', null);
       // scrollHastaAncla("btn-preg3",500,200);
    	break;
    case "preg1d":
    	paso='8';
    	if($('#esCliente').val() === 'S'){
    		$('.preguntas').hide();
        	$('#preg1').show();
        	$('#preg1').attr('data-selected','preg1d');
        	$('#preg5').show();
        	selectSubtituloPresupuesto();
    	}else{
    		$('.preguntas').hide();
        	$('#preg1').show();
        	$('#preg1').attr('data-selected','preg1d');
        	$('#solicita').fadeIn('slow', null);
        	//scrollHastaAncla("div-nombre", 500 , 200);
    	}
    	break;
    default:
    	break;
    }
}

function seleccionarPreguntaSelDistancia(key) {
	switch(key) {
    case "preg1a":
    	paso='8';
    	$('#preg2a').hide();
    	$('#preg2b').show();
    	$(".contenedor-smartMobility-distancia").fadeIn('slow', null);
        //scrollHastaAncla("sel-distancia",500,200);
    	break;
    case "preg1b":
    case "preg1c":
    	paso='4';
    	$('#preg2a').hide();
    	$('#preg2b').show();
        $(".contenedor-smartMobility-distancia").fadeIn('slow', null);
       // scrollHastaAncla("sel-distancia",500,200);
    	break;
    default:
    	break;
    }
}

function bordesRecarga(){
    for (var i = 1; i < 5; i++) {
        var recarga = ".recarga"+i;
        $(recarga).click(function(){
            //quitar la clase selected remove class
            $(".list-unstyled .dispositivos span").removeClass("selected");
            //poner el selected a la clase que lo a clicado
            $(this).addClass("selected");
            $('#inputPuntoRecarga').val($(this).attr('data-value'));
        });
    }
}
function listenerSelectDistancia(){
    $('#distanciaContadorGaraje').change(function(){
    	setValoresTexto();
    });
}

function listenerSelectPlantaContador(){
    $('#plantaContador').change(function(){
    	if(paso !== $(this).attr('data-block')){
    		eliminarSiguientes($(this).attr('data-block'));
    	}
    });
}
function listenerSelectPlantaGaraje(){
    $('#plantaGaraje').change(function(){
    	if(paso !== $(this).attr('data-block')){
    		eliminarSiguientes($(this).attr('data-block'));
    	}
    });
}

function showNextStep(element, scrollParams){
	$(element).fadeIn("slow", null);
   // scrollHastaAncla(scrollParams.element, scrollParams.top, scrollParams.speed);
}

//pre-carga de los paneles de SI o NO
function presupuestoAMedida(){
    for (var i = 1; i < 3; i++) {
    	var presupuesto = ".presupuesto"+i;
        $(presupuesto).click(function(){loopPresupuestoAMedida($(this));});
    }
}

function loopPresupuestoAMedida(att){
    //quitar la clase selected remove class
    $(".list-unstyled .presupuesto span").removeClass("selected");
    //poner el selected a la clase que lo a clicado
    $(att).addClass("selected");
    if($(att).attr('data-value')==="no"){
    	presupuestoAMedidaSelected = "No";
        $('#inputPresupuestoMedida').val($(att).attr('data-value'));
    } else {
    	presupuestoAMedidaSelected = "Si";
        $('#inputPresupuestoMedida').val($(att).attr('data-value'));
    }
}

function precioDesplegable(){
    $(".titulo-precio-oculto").click(function(){
    	animacionImagenPrecios(arrow_grados);
    	if (arrow_grados === 90) {
    		$("#precio-oculto").fadeIn("slow",null);
    		$("#precio-oculto").slideDown("slow");
    		arrow_grados = 0;
    	} else {
    		$("#precio-oculto").slideUp(300);
    		arrow_grados = 90;
    	}
    });
}

function setValoresTexto() {
	$('.p-recarga').each(function(){
		$(this).html($('#inputPuntoRecarga').val());
	});
	$('#txt-metros').html(setRangoMetros());
	if ('0' !== $("#plantaContador option:selected").val() || '0' !== $("#plantaGaraje option:selected").val()){
		$('#txt-calas').html(Math.abs(parseInt($("#plantaContador option:selected").text())-parseInt($("#plantaGaraje option:selected").text())));
	} else {
		$('#txt-calas').parent().remove();
	}
}

function setRangoMetros() {
	var str = $('#distanciaContadorGaraje option:selected').html().split("-");
	var min = parseInt(str[0]);
	var max = parseInt(str[1].split(" ")[0]);
	if ('0' !== $("#plantaContador option:selected").val() || '0' !== $("#plantaGaraje option:selected").val()){
		return (Math.abs(parseInt($("#plantaContador option:selected").text())-parseInt($("#plantaGaraje option:selected").text()))*3 + min) + " - " + (Math.abs(parseInt($("#plantaContador option:selected").text())-parseInt($("#plantaGaraje option:selected").text()))*3 + max);
	}
	return min + " - " + max;
}


function selectSubtituloPresupuesto(){
	if($('#preg5').attr('style', 'display: block;')){
		$('#p-subtituloPresup').remove();
		var textoSubtitulo=selectPresupuestoClientesTexto($('html').attr('lang'));
		$(textoSubtitulo).appendTo('.div-subtituloPresup');
	}
}

function selectPresupuestoClientesTexto(lang) {
	var t_cala;
	if($('#esCliente').val() === 'S' && $('#inuptTipoVivienda').val() === '1D'){
		switch(lang) {
		case 'es':
			t_cala = '<p>Con la informaci&oacute;n que nos facilitas no podemos calcularte un estimado, pero te mandamos un t&eacute;cnico para hacerte un presupuesto a medida si lo deseas.</p>';
			break;
		case 'eu':
			t_cala = '<p>Con la informaci&oacute;n que nos facilitas no podemos calcularte un estimado, pero te mandamos un t&eacute;cnico para hacerte un presupuesto a medida si lo deseas._EU</p>';
			break;
		case 'en':
			t_cala = '<p>Con la informaci&oacute;n que nos facilitas no podemos calcularte un estimado, pero te mandamos un t&eacute;cnico para hacerte un presupuesto a medida si lo deseas._EN</p>';
			break;
		default:
			t_cala = '<p>Con la informaci&oacute;n que nos facilitas no podemos calcularte un estimado, pero te mandamos un t&eacute;cnico para hacerte un presupuesto a medida si lo deseas.</p>';
			break;
		}
	}else{
		switch(lang) {
		case 'es':
			t_cala = '<p>Para recibir un presupuesto definitivo hace falta que un instalador vaya a la vivienda del cliente</br>�Est&aacute; el cliente interesado en que le visite un instalador?</p>';
			break;
		case 'eu':
			t_cala = '<p>Behin betiko aurrekontua jasotzeko, beharrezkoa da instalatzaile bat joatea bezeroaren etxebizitzara</br>Interesatzen zaio bezeroari instalatzaile batek bisitatzea?</p>';
			break;
		case 'en':
			t_cala = '<p>Para recibir un presupuesto definitivo hace falta que un instalador vaya a la vivienda del cliente</br>�Est&aacute; el cliente interesado en que le visite un instalador?_EN</p>';
			break;
		default:
			t_cala = '<p>Para recibir un presupuesto definitivo hace falta que un instalador vaya a la vivienda del cliente</br>�Est&aacute; el cliente interesado en que le visite un instalador?</p>';
			break;
		}
	}
	return t_cala;
}

function cargaInicialPreguntas() {
	$('.preguntas').hide();
}

function listenerBotonEstimacion() {
	$('#btn-estimacion').click(function(){
		$('#sel-distancia').hide();
		$('#preg3').hide();
		$('#preg4').hide();
		$('#recarga').hide();
		$('#precio').hide();
		$('#preg5').hide();
		$('#solicita').hide();
		$('#preg1').fadeIn('slow', null);
		//scrollHastaAncla("preg1", 500 , 200);
	});
}

function listenerBotonSinEstimacion() {
	$('#btn-sin-estimacion').click(function(){
		$('.preguntas').hide();
		eliminarSiguientes("0");
		$('#solicita').fadeIn('slow', null);
		//scrollHastaAncla("div-telefono", 500 , 700);
		 $('#inuptTipoVivienda').val('SE');
	});
}

function listenerClientes() {
		$('#sel-distancia').hide();
		$('#preg3').hide();
		$('#preg4').hide();
		$('#recarga').hide();
		$('#precio').hide();
		$('#preg5').hide();
		$('#solicita').hide();
		$('#preg1').fadeIn('slow', null);
		//scrollHastaAncla("preg1", 500 , 200);
}

function eliminarSiguientes(step) {
	var index = parseInt(step);
	for(var i=(index+1); i<9; i++) {
		$('.bloque'+i).hide();
		resetDatosBloque($('.bloque'+i));
	}
}

function resetDatosBloque(bloque){
	$(bloque).find(".volatilData").each(function(){
		switch($(this).prop("tagName")){
			case "SELECT":
				$(this).val(0);
				if($(".selectpicker").length > 0){
					$('.selectpicker').selectpicker('refresh');
				}
				break;
			case "BUTTON":
				$("#"+$(this).attr("data-id")).val(0);
				if($(".selectpicker").length > 0){
					$('.selectpicker').selectpicker('refresh');
				}
				break;
			case "INPUT":
				$(this).val("");
				break;
			case "SPAN":
				$(this).removeClass("selected");
				break;
			default:
		}
	});
}

function mostrarPresupuesto() {
	$(".contenedor-smartMobility-presupuesto").fadeIn("slow", null);
	$(".contenedor-smartMobility-precio").fadeIn("slow", null);
	paso='5';
	//scrollHastaAncla("precio", 500, 200);
}

function listenerBtnVolverAIntentarAjax() {
	$('#btn-ko-ajax').on('click', function () {
		ajaxPedirPresupuesto(recogerValores());
	});
}

function recogerValores() {
	var final = '"}';
	var obj = '{';
	obj = obj.concat('"tipoVivienda":"'+ $('#inuptTipoVivienda').val());
	if("" !== $('#puntoRecarga').val()){
		obj = obj.concat('","puntoRecarga":"' + $('#inputPuntoRecarga').val());
	}
	if('1A' !== $('#inuptTipoVivienda').val() && '1D' !== $('#inuptTipoVivienda').val()){
		obj = obj.concat('","plantaContador":"' + mapPlantaContador());
		obj = obj.concat('","plantaGaraje":"' + mapPlantaGaraje());
	}
	if("" !== $('#inputPresupuestoMedida').val()){
		obj = obj.concat('","presupuestoMedida":"' + $('#inputPresupuestoMedida').val());
	}
	if(null !== $('#distanciaContadorGaraje').val()){
		obj = obj.concat('","distanciaContadorGaraje":"' + $('#distanciaContadorGaraje').val());
	}
	if(null !== $('#idSolicitud').val()){
		obj = obj.concat('","idSolicitud":"' + $('#idSolicitud').val());
	}
	if("" !== $('#ajax-precio-final').text()){
		obj = obj.concat('","precioFinal":"' + $('#ajax-precio-final').text());
	}
	obj=obj.concat(final);
	return obj;
}

function ajaxPedirPresupuesto(obj) {
	$.ajax({
		url: context + URL_OBTENER_PRESUPUESTO,
		data : obj,
		type:'POST',
		contentType: 'application/json',
		success: function(data) {
			$('.tit-precio-estimado').empty();
			$('#ajax-precio-final').append(comprobarDecimales(data.pvp));
			$('#ajax-precio-precarga').append(data.ppr);
			$('#ajax-precio-instalacion').append(comprobarDecimales(data.pins));
			$('#precioMostradoIncluye').append(data.texto.replace(/##/g,'"').trim());
			setValoresTexto();
			mostrarPresupuesto();
			},
		error: function() {
			$('#modal-ko-ajax').modal('show');
			},
		complete: function() {
			}
	});
}

function comprobarDecimales(num) {
	var values = num.split('-');
	var ret = "";
	for (var i=0;i<values.length;i++){
		values[i].trim();
		var tmp = parseFloat(values[i]);
		if (tmp%1 === 0) {
			ret += Math.floor(tmp) + "&euro;";
		} else {
			ret += tmp + "&euro;";
		}
		if (i===0){
			ret += " - ";
		}
	}
	return ret;
}

function validarCodigoPostal(cap){
	if(!isNaN(cap)){
		if(cap.length === 4){
			cap = "0" + cap;
		}
		if(cap.length === 5){
			var abrev = cap.substring(0, 2);
			if (abrev > '00' && abrev < '53' ){
				cpValido = true;
			}else{
				cpValido = false;
			}
		}else{
			cpValido = false;
		}
	}else{
		cpValido = false;
	}
}

function crearListenerCp(inputCp, listadoCp){
	 $('#' + inputCp).on('keyup paste', function(){
		 	if( $(this).val().length === 3 || $('#' + listadoCp).find('li').length === 0) {
		 		if($('#' + listadoCp).find('li').length > 0){
		 			filtrarListadoFlotante(listadoCp,$(this).val());
		 		}
		 		else{
		 			ajaxObtenerDatosCap(obtenerDatosCp($(this).val()),listadoCp);
		 		}
			}else if($(this).val().length > 3){
				filtrarListadoFlotante(listadoCp,$(this).val());
				$('#' + listadoCp).parent().show();
			}else{
				$('#' + listadoCp).children().remove();
				$('#' + listadoCp).parent().hide();
			}
	});
}
function crearListenerCiudad(inputCiudad, listadoCiudad){
	 $('#' + inputCiudad).on('keyup', function(){
		if ($(this).val()!=="") {
			filtrarListadoFlotante(listadoCiudad,$(this).val());
			$('#' + listadoCiudad).parent().show();
		}
	});
}


function crearListaFlotanteCiudad(datos,idLista) {
	var li;
	var a;
	$.each( datos, function( index, value ) {
		if(value.localidad!==undefined && value.localidad!=='') {
			li = $('<li>');
			a = $('<a tabindex="0">').html(escapeHtml(value.localidad));
			a.appendTo(li);
			li.appendTo('#' + idLista);
		}
	});
	if(datos.length > 1){
		$('#' + idLista).parent().show();
	}else{
		if(datos.length === 1){
			$('#' + idLista + ' li:first-child a').click();
		}
		$('#' + idLista).parent().hide();
	}
}

function obtenerDatosCp(txt) {
	//Datos a enviar
	var jsonCap = {
			"cap":txt.toUpperCase()
			};
	return jsonCap;
}

function ajaxObtenerDatosCap(json, listadoCp) {
	$('#smartMobility-codigoPostal').attr('readonly', true);
	loaderIn();
	 $.ajax({
		type: "POST",
		url: context + URL_OBTENER_CAP,
		data: JSON.stringify(json),
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		async: true,
		success: function() {
			$('#' + listadoCp).children().remove();
			loaderOut();
			$('#smartMobility-codigoPostal').attr('readonly', false);
		},
		error: function()  {
			loaderOut();
			$('#smartMobility-codigoPostal').attr('readonly', false);
		}
	});
}

function ajaxEnviarPreguntasYRespuestas(obj) {
	$.ajax({
		url: context + URL_OBTENER_GUARDAR_PREGUNTAS,
		data : obj,
		type:'POST',
		contentType: 'application/json',
		success: function() {
			},
		error: function() {
			}
	});
}

function ajaxObtenerDatosCiudad(json, listaCiudad) {
	 loaderIn();
		$.ajax({
			type: "POST",
			url: context + URL_OBTENER_PROVINCIAS,
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			async: true,
			success: function(data) {
				$('#' + listaCiudad).children().remove();
				crearListaFlotanteCiudad(data,listaCiudad);
				loaderOut();
			},
			error: function()  {
				loaderOut();
			}
		});
}

function clickCap(elemento, listado, inputCiudad, listadoCiudad){
	 $('#' + listado).parent().prev('.inputs-form').val(elemento.innerHTML);
	 $('#' + listado).parent().hide();
	 $('#' + inputCiudad).attr('readonly', false);
	 ajaxObtenerDatosCiudad(obtenerDatosCp(elemento.innerHTML),listadoCiudad);
}

function clickCiudad(elemento, listaCiudad){
	 $('#' + listaCiudad).parent().prev('.inputs-form').val(elemento.innerHTML);
	 $('#' + listaCiudad).parent().hide();
}

function mostrarCaminoVuelta() {
	if(("" !== $("#inuptTipoVivienda").val() && undefined !== $("#distanciaContadorGaraje").val() 
		&& "0" !== $("#distanciaContadorGaraje").val()) || ($("#inuptTipoVivienda").val() === "SE")) {
		switch($("#inuptTipoVivienda").val()){
			case '1A':
				$('#preg1').show();
				$('#sel-distancia').show();
				$('#recarga').show();
				$('#precio').show();
				$('#preg5').show();
				$('#solicita').show();
				break;
			case '1B':
			case '1C':
				$('#preg1').show();
				$('#preg3').show();
				$('#preg4').show();
				$('#sel-distancia').show();
				$('#recarga').show();
				$('#precio').show();
				$('#preg5').show();
				$('#solicita').show();
				break;
			case '1D':
				$('#preg1').show();
				$('#preg5').show();
				selectSubtituloPresupuesto();
				$('#solicita').show();
				break;
			case 'SE':
				$('#solicita').show();
				//scrollHastaAncla('solicita', 100);
				break;
			default:
				break;
		}
	}
	//scrollHastaAncla('solicita',200);
}

function marcarErroresConAncla() {
	var detectadoPrimerError = false;
	var errorDetectado = false;
    $('.help-block').each(function(){
        if($(this).html()!=='') {
        	$(this).removeClass("hidden");
            $(this).parent().addClass('has-error');
            if(!detectadoPrimerError){
	            $(this).parent().find('.form-control').each(function(){
	            	if($(this).attr('id') !== undefined){
	            		$(this).focus();
	            		detectadoPrimerError = true;
	            	}
	    		});
            }
            errorDetectado = true;
        } else {
        	$(this).addClass("hidden");
            $(this).parent().removeClass('has-error');

        }
    });
    if(!errorDetectado){
    	$("input[type='checkbox']").each(function(){
            if($(this).parent().hasClass("has-error") && $(this).attr('id')!== undefined) {
        		//scrollHastaAncla($(this).attr('id'), null, 50);
            }
    	});
    }
}

function listenerModalReSend() {
	$('#btn-reenviar-sms').on('click', function(){
		$('#btn-reenviar-sms').remove();
		var boton_reenviar=selectIdiomaReenviar($('html').attr('lang'));
		$(boton_reenviar).appendTo('.label-reenviar');
	    listenerReSendButton();
	});
}

function selectIdiomaReenviar(lang) {
	var val;
	switch(lang) {
	case 'es':
		val = '<span>Puedes pulsar <a role="button" class="a-label-reenviar" id="reSendSms">aqu&iacute;</a> para enviar un nuevo SMS al cliente.</span>';
		break;
	case 'eu':
		val = '<span><a role="button" class="a-label-reenviar" id="reSendSms">Hemen</a> sakatu dezakezu bezeroari SMS berri bat bidaltzek.</span>';
		break;
	case 'en':
		val = '<span>Click <a role="button" class="a-label-reenviar" id="reSendSms">here</a> to send a new SMS to the customer.</span>';
		break;
	default:
		val = '<span>Puedes pulsar <a role="button" class="a-label-reenviar" id="reSendSms">aqu&iacute;</a> para enviar un nuevo SMS al cliente.</span>';
		break;
	}
	return val;
}

function listenerReSendButton() {
	$('#reSendSms').on('click', function(){
		if(!formularioEnviado){
			$('#valueBoton').val('reSendSms');
			$('#form-smart-mobility-datosPers').submit();
			formularioEnviado=true;
		}
	});
}

function listenerSendButton() {
	$('#boton-enviar-sms').on('click', function(){
		if(!formularioEnviado){
			$('#valueBoton').val('check');
			formularioEnviado=true;
		}
	});
}

function mostrarCaminoConDatos() {
	if($('#distanciaContadorGaraje').val() !== undefined && $('#distanciaContadorGaraje').val() !== '0') {
		switch($('#inuptTipoVivienda').val()){
			case '1A':
					$('#preg1a').addClass('selected');
					$('preg2a').show();
					$('preg2b').hide();
					selectedPuntoRecarga($('#inputPuntoRecarga').val());
					selectedSiONo($('#inputPresupuestoMedida').val());
				break;
			case '1B':
					$('#preg1b').addClass('selected');
					$('preg2b').show();
					$('preg2a').hide();
					selectedPuntoRecarga($('#inputPuntoRecarga').val());
					selectedSiONo($('#inputPresupuestoMedida').val());
				break;
			case '1C':
					$('#preg1c').addClass('selected');
					$('preg2b').show();
					$('preg2a').hide();
					selectedPuntoRecarga($('#inputPuntoRecarga').val());
					selectedSiONo($('#inputPresupuestoMedida').val());
				break;
			case '1D':
				if($('#esCliente').val() === 'S'){
					$('#preg1d').addClass('selected');
					$('preg5').show();
					selectSubtituloPresupuesto();
					selectedSiONo($('#inputPresupuestoMedida').val());
				}else{
					$('#preg1d').addClass('selected');
				}
				break;
			default:
				break;
		}
	}
}

function selectedPuntoRecarga(puntoRecarga) {
	switch(puntoRecarga){
	case 'Home':
			$('.recarga1').addClass('selected');
		break;
	case 'Pulsar':
			$('.recarga2').addClass('selected');
		break;
	case 'Commander':
			$('.recarga3').addClass('selected');
		break;
	case 'Copper':
			$('.recarga4').addClass('selected');
		break;
	default:
		break;
	}
}

function selectedSiONo(SiNo) {
	if (SiNo === 'si'){
		$('.presupuesto1').addClass('selected');
	} else {
		$('.presupuesto2').addClass('selected');
	}
}

function animacionImagenPrecios(grados) {
	$('#arrow-precios').animate({borderSpacing: grados}, {
	    step: function(now)
	    {
	      $(this).css('-webkit-transform','rotate('+now+'deg)');
	      $(this).css('-moz-transform','rotate('+now+'deg)');
	      $(this).css('transform','rotate('+now+'deg)');
	    }
	,duration:'slow'}
	,'linear');
}

function mapPlantaGaraje(){
	switch($('#plantaGaraje').val()) {
	case '0':
		return "-10";
	case '1':
		return "-3";
	case '2':
		return "-2";
	case '3':
		return "-1";
	case '4':
		return "0";
	case '5':
		return "1";
	case '6':
		return "2";
	case '7':
		return "3";
	default:
		return "-10";
	}
}

function mapPlantaContador(){
	switch($('#plantaContador').val()) {
	case '0':
		return "-10";
	case '1':
		return "00";
	case '2':
		return "-3";
	case '3':
		return "-2";
	case '4':
		return "-1";
	case '5':
		return "0";
		break;
	case '6':
		return "1";
	case '7':
		return "2";
	case '8':
		return "3";
	default:
		return "-10";
	}
}

