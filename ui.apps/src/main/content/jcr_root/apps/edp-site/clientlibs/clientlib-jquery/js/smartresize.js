/*!
 * jQuery Smartresize Function
 * Intermark Tecnologías
 * 
 * Lanza la respuesta al evento no cuando se está produciendo sino cuando se termina de ejecutar la acción
 * 
 * Versión: 1.0.0
 */
(function(c, b) {
	var a = function(g, d, e) {
		var h;
		return function f() {
			var k = this, j = arguments;
			function i() {
				if (!e) {
					g.apply(k, j)
				}
				h = null
			}
			if (h) {
				clearTimeout(h)
			} else {
				if (e) {
					g.apply(k, j)
				}
			}
			h = setTimeout(i, d || 100)
		}
	};
	jQuery.fn[b] = function(d) {
		return d ? this.bind("resize", a(d)) : this.trigger(b)
	}
})(jQuery, "smartresize");