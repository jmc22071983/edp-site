(function(c) {
	c.cookiesDirective = function(j) {
		var k = c
				.extend(
						{
							explicitConsent : true,
							position : "top",
							duration : -1,
							limit : 0,
							message : 'We use own and third party cookies to improve our services. Pressing the <strong>Accept</strong> consider accepting their use. You can get more information or how to change the settings in our <a href="{{privacyPolicyUri}}">{{privacyPolicyText}}</a>.',
							privacyPolicyText : "Privacy policy",
							privacyPolicyUri : "/privacy-policy/",
							explicitSubmitText : "Accept",
							impliedSubmitText : "Close",
							cookieExpires : 365,
							cookiePath : "/",
							styleWrapper : "",
							scriptWrapper : function() {
							}
						}, j);
		if (!a("cookiesDirective")) {
			var f = window.location.pathname;
			var i = "";
			if ((f.indexOf(k.privacyPolicyUri) < 0)
					&& (!a("cookiesDirectiveLocation"))) {
				try {
					b("cookiesDirectiveLocation", f, {
						expires : 1,
						path : k.cookiePath
					})
				} catch (h) {
					console.log(h.message)
				}
			} else {
				i = a("cookiesDirectiveLocation")
			}
			if (!a("cookiesDisclosureCount")) {
				b("cookiesDisclosureCount", 0, {
					expires : 1,
					path : k.cookiePath
				})
			}
			var g = a("cookiesDisclosureCount");
			if (f.indexOf(k.privacyPolicyUri) >= 0) {
			} else {
				if (f == i || i == "") {
					g = 1
				} else {
					g++
				}
			}
			b("cookiesDisclosureCount", g, {
				expires : 1,
				path : k.cookiePath
			});
			if (!k.explicitConsent && a("cookiesDisclosureCount") > 1) {
				b("cookiesDirective", 1, {
					expires : k.cookieExpires,
					path : k.cookiePath
				});
				d("cookiesDirectiveLocation", {
					path : k.cookiePath
				});
				k.scriptWrapper.call()
			} else {
				if (k.limit > 0) {
					if (k.limit >= a("cookiesDisclosureCount")) {
						e(k)
					}
				} else {
					e(k)
				}
			}
		} else {
			k.scriptWrapper.call()
		}
	};
	var e = function(f) {
		var h = f;
		var g = "";
		g += '<div id="epd" class="' + h.styleWrapper + '">';
		g += '<div id="cookiesdirective" class="cookiesdirective">';
		g += "<div>";
		if (h.message) {
			g += h.message.replace("{{privacyPolicyUri}}", h.privacyPolicyUri)
					.replace("{{privacyPolicyText}}", h.privacyPolicyText)
		}
		if (h.explicitConsent) {
			g += '<div><input type="submit" name="explicitsubmit" id="explicitsubmit" value="'
					+ h.explicitSubmitText + '"/></div>'
		} else {
			g += '<div><input type="submit" name="impliedsubmit" id="impliedsubmit" value="'
					+ h.impliedSubmitText + '"/></div>'
		}
		g += "</div></div></div>";
		c("body").addClass("cookies").prepend(g);
		var j = h.position.toLowerCase();
		if (j != "top" && j != "bottom") {
			j = "top"
		}
		var i = new Array();
		if (j == "top") {
			i["in"] = {
				opacity : "1"
			};
			i.out = {
				opacity : "0"
			}
		} else {
			i["in"] = {
				opacity : "1"
			};
			i.out = {
				opacity : "0"
			}
		}
		c("#cookiesdirective")
				.animate(
						i["in"],
						1000,
						function() {
							if (h.explicitConsent) {
								c("#explicitsubmit").click(
										function() {
											b("cookiesDirective", 1, {
												expires : h.cookieExpires,
												path : h.cookiePath
											});
											c("#cookiesdirective").animate(
													i.out,
													1000,
													function() {
														c("body").removeClass(
																"cookies");
														c("#cookiesdirective")
																.remove();
														location.reload(true)
													})
										})
							} else {
								c("#impliedsubmit").click(
										function() {
											b("cookiesDirective", 1, {
												expires : h.cookieExpires,
												path : h.cookiePath
											});
											c("#cookiesdirective").animate(
													i.out,
													1000,
													function() {
														c("body").removeClass(
																"cookies");
														c("#cookiesdirective")
																.remove()
													})
										});
								c("a")
										.click(
												function() {
													var k = c(this)
															.attr("href");
													if (k != undefined
															&& k != null
															&& k
																	.indexOf(h.privacyPolicyUri) < 0) {
														var m = location.host
																.toString();
														var l = new RegExp(
																"^(https?:)?//"
																		+ m,
																"i");
														if (l.test(k)
																|| /^\//
																		.test(k)
																|| /^\.\//
																		.test(k)
																|| /^\.\.\//
																		.test(k)
																|| /^\#/
																		.test(k)) {
															b(
																	"cookiesDirective",
																	1,
																	{
																		expires : h.cookieExpires,
																		path : h.cookiePath
																	})
														}
													}
												})
							}
							if (h.duration >= 0) {
								setTimeout(function() {
									c("#cookiesdirective")
											.animate(
													i.out,
													1000,
													function() {
														c("body").removeClass(
																"cookies");
														c("#cookiesdirective")
																.remove()
													})
								}, h.duration * 1000)
							}
						})
	};
	c.cookiesDirective.loadScript = function(g) {
		var h = c.extend({
			uri : "",
			appendTo : "body"
		}, g);
		var f = String(h.appendTo);
		var i = document.createElement("script");
		i.src = h.uri;
		i.type = "text/javascript";
		i.onload = i.onreadystatechange = function() {
			if ((!i.readyState || i.readyState == "loaded" || i.readyState == "complete")) {
				return
			}
		};
		switch (h.appendTo) {
		case "head":
			c("head").append(i);
			break;
		case "body":
			c("body").append(i);
			break;
		default:
			c("#" + f).append(i)
		}
	};
	var a = function(f) {
		return c.cookie(f)
	};
	var b = function(g, h, f) {
		c.cookie(g, h, f)
	};
	var d = function(g, f) {
		c.removeCookie(g, f)
	}
})(jQuery);