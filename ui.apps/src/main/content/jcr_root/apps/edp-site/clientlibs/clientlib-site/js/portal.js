var portal = {
	inicializar : function() {
		portal.detectarNavegador();
		portal.desactivarAutocomplete();
		portal.cargarUtilidades();
		portal.activarBuscadorGeneral();
		portal.ampliarImagenes();
		portal.enlacesExternos();
		portal.aceptarCookies();
		portal.cargarBookmark();
		portal.cargarTooltip();
		portal.cargarReproductorMedia();
		portal.cargarCaptcha();
		portal.cargarOembed();
		portal.cargarMapaAreas();
		portal.cargarCSRFToken()
	},
	detectarNavegador : function() {
		var b = "";
		var a = $.browser.version.split(".");
		if ($.browser.mozilla) {
			b = "mozilla moz"
		}
		if ($.browser.webkit) {
			b = "webkit webkit"
		}
		if ($.browser.msie) {
			b = "ie ie"
		}
		if ($.browser.opera) {
			b = "opera opera"
		}
		if (b != "") {
			$("html").addClass(b + a[0])
		}
	},
	desactivarAutocomplete : function() {
		$("input.autocomplete-off").each(function() {
			$(this).attr("autocomplete", "off")
		})
	},
	cargarUtilidades : function() {
		$("#atajo ul li").last().not("#atajoVolver").before(
				'<li id="atajoVolver"><a href="#volver"><img src="'
						+ contextoPortal
						+ '/public/img/comun/icono/atajoVolver.png" alt="'
						+ texto.volver + '" /></a></li>\n');
		$("#atajoVolver a").click(function() {
			portal.volver();
			return false
		});
		$("#utilidades li").last().not("#servicioImprimir").removeClass(
				"ultimo").after(
				'<li class="ultimo" id="servicioImprimir"><a href="#imprimir"><img src="'
						+ contextoPortal
						+ '/public/img/comun/icono/imprimir.png" alt="" />'
						+ texto.imprimir + "</a></li>");
		$("#servicioImprimir a").click(function() {
			portal.imprimir();
			return false
		})
	},
	activarBuscadorGeneral : function() {
		if ($("#buscadorGeneralForm")) {
			$("#buscadorGeneralForm").submit(function() {
				return portal.validarBuscadorGeneral(this)
			})
		}
		$("#textobusqueda")
				.autocomplete(
						{
							appendTo : "#buscadorGeneralForm div",
							source : function(b, a) {
								var c = util.reemplazarCaracteresDiacriticos(
										b.term).toLowerCase();
								$
										.ajax({
											url : contextoPortal
													+ "/rest/buscador/sugerencias/"
													+ codigoIdioma + "/"
													+ encodeURIComponent(c),
											dataType : "json",
											success : function(d) {
												if (d == null) {
													a([ texto.noSugerenciasBuscadorGeneral ])
												} else {
													if ($.isArray(d.resultados)) {
														a($
																.map(
																		d.resultados,
																		function(
																				e) {
																			return {
																				label : e.sugerencia,
																				value : e.sugerencia
																			}
																		}))
													} else {
														a([ d.resultados.sugerencia ])
													}
												}
											}
										})
							},
							minLength : 2,
							delay : 300,
							select : function(a, b) {
								$("#buscadorGeneralForm")
										.addClass("sugerencia");
								if (b.item.value == texto.noSugerenciasBuscadorGeneral) {
									return false
								}
							},
							open : function(a, b) {
								$("#buscadorGeneralForm").css("height",
										$(".ui-autocomplete").height() + 50)
							},
							close : function(a, b) {
								$("#buscadorGeneralForm").css("height", "auto")
							}
						});
		if ($("#textobusqueda")) {
			this.textoBuscador = $("#textobusqueda").val();
			$("#textobusqueda").focus(function() {
				$(this).val("")
			})
		}
	},
	ampliarImagenes : function() {
		$("img.adjunto_si")
				.each(
						function() {
							$(this).on("click keypress", function() {
								portal.verImagen(this)
							});
							$(this).addClass("cursorAdjunto");
							if (typeof texto.altPopupImagen != "undefined") {
								if (($(this).attr("alt") != "")
										&& ($(this).attr("alt").indexOf(
												texto.altPopupImagen) == -1)) {
									$(this)
											.attr(
													"alt",
													($(this).attr("alt") + " " + texto.altPopupImagen))
								} else {
									$(this).attr("alt", texto.altPopupImagen)
								}
								$(this).attr("title", $(this).attr("alt"))
							}
						})
	},
	enlacesExternos : function() {
		$("a[href][rel$='external']")
				.each(
						function() {
							$(this).attr("target", "_blank");
							if (!$(this).hasClass("tooltip")) {
								if (!$("img", this).hasClass("tooltip")) {
									if (!$(this).attr("title")
											|| $(this).attr("title") == "undefined"
											|| $(this).attr("title") == "") {
										$(this).attr("title",
												texto.titleVentanaNueva)
									} else {
										$(this)
												.attr(
														"title",
														$(this).attr("title")
																+ " ("
																+ texto.titleVentanaNueva
																+ ")")
									}
								}
							}
						});
		$("area[href][class$='external']").each(
				function() {
					$(this).attr("target", "_blank");
					if (!$(this).attr("title")
							|| $(this).attr("title") == "undefined"
							|| $(this).attr("title") == "") {
						$(this).attr("title", texto.titleVentanaNueva)
					} else {
						$(this).attr(
								"title",
								$(this).attr("title") + " ("
										+ texto.titleVentanaNueva + ")")
					}
				})
	},
	imprimir : function() {
		window.print()
	},
	volver : function() {
		history.go(-1)
	},
	validarBuscadorGeneral : function(a) {
		if (validateBuscadorGeneralForm(a)) {
			if ($("#textobusqueda").val() == this.textoBuscador) {
				alert(texto.validacionBuscadorGeneral);
				$("#textobusqueda").val("");
				$("#textobusqueda").focus();
				return false
			}
			return true
		}
		return false
	},
	verImagen : function(a) {
		if (typeof (imagen) != "undefined") {
			imagen.ampliar(a)
		}
	},
	aceptarCookies : function() {
		$.cookiesDirective({
			explicitConsent : false,
			message : politicaCookies.mensaje,
			privacyPolicyText : politicaCookies.titulo,
			privacyPolicyUri : contextoPortal + "/" + codigoIdioma + "/"
					+ politicaCookies.enlace,
			impliedSubmitText : politicaCookies.boton,
			cookiePath : contextoPortal + "/"
		})
	},
	cargarBookmark : function() {
		if ($.fn.sharrre) {
			$(".compartir-sharrre").sharrre({
				share : {
					facebook : true,
					twitter : true,
					googlePlus : true
				},
				buttons : {
					facebook : {
						layout : "box_count"
					},
					twitter : {
						count : "vertical",
						via : ""
					},
					googlePlus : {
						size : "tall",
						urlCurl : ""
					}
				},
				hover : function(b, a) {
					$(b.element).find(".buttons").show()
				},
				hide : function(b, a) {
					$(b.element).find(".buttons").hide()
				},
				enableTracking : true
			})
		}
	},
	cargarTooltip : function() {
		$(document).tooltip({
			items : "[title], img[alt], input[alt]",
			content : function() {
				var a = $(this);
				if (a.is("[title]")) {
					return a.attr("title")
				}
				if (a.is("img[alt], input[alt]")) {
					return a.attr("alt")
				}
			},
			position : {
				my : "left bottom",
				at : "center top"
			}
		})
	},
	cargarReproductorMedia : function() {
		if ($.fn.mediaelementplayer) {
			mejs.i18n.language(codigoIdioma);
			$(".reproductorMedia.mediaelement").each(
					function() {
						var id = $(this).attr("id");
						if ((id != null) && (id != "")) {
							var opciones = eval(id);
							var defaults = {
								pluginPath : contextoPortal
										+ "/public/media/mediaelementjs/",
								features : [ "playpause", "current",
										"progress", "time", "tracks", "volume",
										"fullscreen", "googleanalytics" ],
								audioVolume : "horizontal",
								videoVolume : "vertical",
								showPosterWhenEnded : true
							};
							var settings = $.extend(defaults, opciones);
							$("#reproductor_" + id)
									.mediaelementplayer(settings)
						}
					})
		}
	},
	cargarCaptcha : function() {
		$("#imagenCaptcha").click(
				function() {
					$(this).attr(
							"src",
							contextoPortal + "/public/img/captcha.png?"
									+ Math.floor(Math.random() * 100))
				});
		$("#etiquetaCaptcha").append(
				'<span class="descripcion">' + texto.nuevoCaptcha + "</span>")
	},
	cargarOembed : function() {
		$(".oembed").oembed(null, {
			includeHandle : false
		})
	},
	cargarMapaAreas : function() {
		$("map").each(function() {
			var a = $(this).attr("id");
			$(this).attr("name", a)
		})
	},
	cargarCSRFToken : function() {
		if (csrfToken != "") {
			$(
					'a[id^="Menu_"], a[id^="Submenu_"], a[id^="Contenido_"], a[id^="Aplicacion_"]')
					.each(
							function() {
								if ($(this).attr("href").indexOf("csrfToken=") == -1) {
									$(this)
											.attr(
													"href",
													$(this).attr("href")
															+ "?csrfToken="
															+ csrfToken)
								}
							})
		}
	}
};