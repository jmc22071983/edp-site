package com.aem.edp.site.core.models.general;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HighlightedContent {
	
	private static final Logger LOG = LoggerFactory.getLogger(HighlightedContent.class);
	
	@Inject
	@Via("resource")
	private String style;
	
	@Inject
	@Via("resource")
	private String ctype;
	
	@Inject
	@Via("resource")
	private String id;
	
	
	@Inject
	@Via("resource")
	private String title;
	
	
	@PostConstruct
	protected void init() {
		LOG.debug("Init HighlightedContent");
		
		LOG.debug("Test jira flow for AEM-2 issue");
		
		LOG.debug("Test Auto jenkins Job");
		
		LOG.debug("End HighlightedContent");
		
		
	}


	public String getStyle() {return style;}


	public String getCtype() {return ctype;}


	public String getId() {return id;}


	public String getTitle() {return title;}

	
	
	
	
	
	
	
}
