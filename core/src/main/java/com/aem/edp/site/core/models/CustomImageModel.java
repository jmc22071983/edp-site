package com.aem.edp.site.core.models;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import com.adobe.cq.wcm.core.components.models.Image;

@Model(adaptables = SlingHttpServletRequest.class, adapters = Image.class, resourceType = "edp-site/components/content/image")
public class CustomImageModel {
	@Self @Via(type = ResourceSuperType.class)
    private Image image;
	
	public Image getImage() {
		return image;
	}
}
