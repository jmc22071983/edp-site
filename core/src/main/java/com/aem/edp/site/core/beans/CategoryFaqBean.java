package com.aem.edp.site.core.beans;

public class CategoryFaqBean {
	
	private String id;
	
	private String path;
	
	private String title;

	public CategoryFaqBean(String id, String path, String title) {
		super();
		this.id = id;
		this.path = path;
		this.title = title;
	}
	
	public String getId() {return id;}

	public String getPath() {return path;}

	public String getTitle() {return title;}
	
}
