package com.aem.edp.site.core.beans;

public class MobiltyBean {

	private String id;
	
	private String type;
	
	private String path;
	
	private String title;
	
	public MobiltyBean(String type, String id, String path, String title) {
		super();
		this.type = type;
		this.id = id;
		this.path = path;
		this.title = title;
	}
	
	public String getType() {return type;}

	public String getId() {return id;}

	public String getPath() {return path;}

	public String getTitle() {return title;}
}
