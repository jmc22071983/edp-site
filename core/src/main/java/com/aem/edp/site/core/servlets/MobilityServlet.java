package com.aem.edp.site.core.servlets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.Servlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import org.apache.sling.api.resource.ResourceMetadata;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=Dynamic Mobility",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.resourceTypes=" + "/apps/getMobility"})
public class MobilityServlet extends SlingSafeMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) {
		
		List<Resource> resourceList = new ArrayList<>();
		if(req.getRequestPathInfo().getSuffix() != null){
			ResourceResolver resolver = req.getResourceResolver();
			
			ValueMap vm1 = new ValueMapDecorator(new HashMap<String, Object>());
			vm1.put("value", "type1");
			vm1.put("text", "Vivienda Unifamiliar");
			
			resourceList.add(new ValueMapResource(resolver,	new ResourceMetadata(), "nt:unstructured", vm1));
			
			ValueMap vm2 = new ValueMapDecorator(new HashMap<String, Object>());
			vm2.put("value", "type2");
			vm2.put("text", "Garaje comunitario");
			
			resourceList.add(new ValueMapResource(resolver,	new ResourceMetadata(), "nt:unstructured", vm2));
			
			//Create a DataSource that is used to populate the drop-down control
			DataSource ds = new SimpleDataSource(resourceList.iterator());
			req.setAttribute(DataSource.class.getName(), ds);
		}
		
	}
}
