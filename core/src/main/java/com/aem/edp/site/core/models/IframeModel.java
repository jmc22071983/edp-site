package com.aem.edp.site.core.models;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class IframeModel {

    private static final Logger LOGGER = LoggerFactory.getLogger(IframeModel.class);

    @SlingObject
    private SlingHttpServletRequest request;

    @Inject
    @Via("resource")
    private String id;

    @Inject
    @Via("resource")
    private String title;


    @Inject
    @Via("resource")
    private String src;

    @Inject
    @Via("resource")
    private String heightDesktop;

    @Inject
    @Via("resource")
    private String heightTablet;

    @Inject
    @Via("resource")
    private String heightMobile;


    @Inject
    @Via("resource")
    private String width;

    @PostConstruct
    protected void init() {
        try {
            if (request != null) {
                LOGGER.info("IframeModel- Info: Inside PostConstruct");
            } else {
                LOGGER.error("IframeModel- Error: Null Request");
            }
        } catch (Exception e) {
            LOGGER.error(String.format("IframeModel- Error:  %s", e.getMessage()), e);
        }
    }

    public static Logger getLOGGER() {
        return LOGGER;
    }

    public SlingHttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(SlingHttpServletRequest request) {
        this.request = request;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getHeightDesktop() {
        return heightDesktop;
    }

    public void setHeightDesktop(String heightDesktop) {
        this.heightDesktop = heightDesktop;
    }

    public String getHeightTablet() {
        return heightTablet;
    }

    public void setHeightTablet(String heightTablet) {
        this.heightTablet = heightTablet;
    }

    public String getHeightMobile() {
        return heightMobile;
    }

    public void setHeightMobile(String heightMobile) {
        this.heightMobile = heightMobile;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
