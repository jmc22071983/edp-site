package com.aem.edp.site.core.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;

public class DocModel {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	@Via("resource")
	private Date date;
	
	@Inject
	@Via("resource")
	private String docpath;
	
	private  String dateString;
	
	private  String formatSize;
	
	private static final String  DEFAULT_DOC = "/content/dam/repsol-site/documents/notas-de-prensa/2019/NP190527-repsol-inaugura-estacion-morelia_tcm120-159741.pdf";
	
	private static final String  DEFAULT_DATE = "31/12/2018 10:00";
	
	
	@PostConstruct
	protected void init() {
		ResourceResolver resolver = request.getResourceResolver();
		if(null != date) {
			DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm"); 
			this.dateString = outputFormat.format(date.getTime());
		}
		if(StringUtils.isEmpty(this.dateString)) {this.dateString = DEFAULT_DATE;}
		if(StringUtils.isEmpty(this.docpath)) {this.docpath = DEFAULT_DOC;}
		Resource re = resolver.getResource(docpath);
		Asset asset = DamUtil.resolveToAsset(re);
		final String size = asset.getMetadataValue("dam:size");
		this.formatSize = DocModel.formatSize(Double.valueOf(size), 2);
		
	}
	
	public static String formatSize(Double bytes, int digits) {
        String[] dictionary = {"bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
        int index = 0;
        for (index = 0; index < dictionary.length; index++) {
            if (bytes < 1024) {
                break;
            }
            bytes = bytes / 1024;
        }
        final String sFormat = "%." + digits + "f";
        String sizeFile = String.format(sFormat, bytes) + " " + dictionary[index];
        return sizeFile.replace(".", ",");
    }


	public String getDocpath() {
		return docpath;
	}

	public void setDocpath(String docpath) {
		this.docpath = docpath;
	}
	
	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public String getFormatSize() {
		return formatSize;
	}

	public void setFormatSize(String formatSize) {
		this.formatSize = formatSize;
	}
}
