package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.edp.site.core.beans.CategoryFaqBean;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class FaqsModel {
	private static final Logger LOG = LoggerFactory.getLogger(FaqsModel.class);
	
	private static final String TAB = "tab-";
	
	@Self
	private SlingHttpServletRequest request;

	private List<CategoryFaqBean> lstCategories = new ArrayList<>();
	
	@PostConstruct
	protected void init() {
		LOG.debug("Init - CategoryFaqBean Post Construct");
		Resource r = request.getResource();
		if (r.hasChildren() && r.getChild("categorylist") != null && null != r.getChild("categorylist").getChildren()) {
			Iterator<Resource> it = r.getChild("categorylist").getChildren().iterator();
			int k = 1;
			while(it.hasNext()) {
				Resource rCategory = it.next();
				ValueMap vMap = rCategory.getValueMap();
				final String path = null != vMap.get("path") ? (String)vMap.get("path") : null;
				final String title = null != vMap.get("title") ? (String)vMap.get("title") : null;
				LOG.debug("Category path: {} , Category title: {}", path, title);
				if(StringUtils.isNotEmpty(path) || StringUtils.isNotEmpty(title)) {
					CategoryFaqBean categoryFaq = new CategoryFaqBean(TAB.concat(String.valueOf(k)),path, title);
					lstCategories.add(categoryFaq);
				}
				k++;
			}
		}	
		LOG.debug("End - CategoryFaqBean Post Construct");
	}

	public List<CategoryFaqBean> getLstCategories() {return lstCategories; }
	
	
}
