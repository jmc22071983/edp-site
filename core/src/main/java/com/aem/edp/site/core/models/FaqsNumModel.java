package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.Resource;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FaqsNumModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(FaqsNumModel.class);
	
	@Self
	private SlingHttpServletRequest request;
	
	
	@Inject
	@Via("resource")
	private String qnums;
	
	private List<String> list = new ArrayList<>();
	
	
	@PostConstruct
	protected void init() {
		if(StringUtils.isNotEmpty(qnums)) {
			int elements = Integer.parseInt(qnums);
			String[] aElem = new String[elements];
			list = Arrays.asList(aElem);
		}
		else {
			list = Arrays.asList(new String[5]);
		}
	}

	public String getQnums() {
		return qnums;
	}



	public void setQnums(String qnums) {
		this.qnums = qnums;
	}



	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	
	

}
