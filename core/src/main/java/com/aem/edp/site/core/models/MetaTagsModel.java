package com.aem.edp.site.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
@Model(adaptables = {SlingHttpServletRequest.class,
        Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MetaTagsModel {
	 private static final Logger LOG = LoggerFactory.getLogger(MetaTagsModel.class);
	    
	    @Inject
	    private Resource resource;

	    @Inject
	    private SlingHttpServletRequest request;

	    @Inject
	    private Page resourcePage;

	    @Inject
	    private Page currentPage;

	    @Inject
	    @Via("resource")
	    private String metaTitle;

	    @Inject
	    @Via("resource")
	    private String metaDescription;

	    @Inject
	    @Via("resource")
	    private String[] metaKeywords;

	    @Inject
	    @Via("resource")
	    private String canonical;

	    @Inject
	    @Via("resource")
	    private String robotNoindex;

	    @Inject
	    @Via("resource")
	    private String robotNofollow;

	    private String metaRobots;

	    private String customMetaKeywords;

	    private String templatePath;

	    private String contentURL;

	    @PostConstruct
	    protected void init() {
	        if (resource != null) {
	            try {
	                templatePath = resourcePage.getTemplate().getPath();
	                if (metaTitle == null) metaTitle = resourcePage.getTitle();
	                if (metaDescription == null) metaDescription = "Repsol Meta Description Default";
	                keywordsTreatment();
	                robotsFactory();
	                contentURLFactory();
	                canonicalFactory();
	            } catch (Exception ex) {
	                LOG.error(String.format("Exception: %s", ex));
	            }
	        }
	    }

	    private void keywordsTreatment() {
	        try {
	            if (metaKeywords != null) {
	                List<String> keys = new ArrayList<>();
	                TagManager tagManager = resource.getResourceResolver().adaptTo(TagManager.class);
	                for (String key : metaKeywords) {
	                    Tag t = tagManager.resolve(key);
	                    if (t != null) {
	                        keys.add(t.getTitle());
	                    }
	                }
	                for (int i = 0; i < keys.size(); i++) {
	                    if (i == 0) {
	                        customMetaKeywords = keys.get(i);
	                    } else {
	                        customMetaKeywords += ", " + keys.get(i);
	                    }
	                }
	            } 
	        } catch (NullPointerException ex) {
	            LOG.error(String.format("Exception: %s", ex));
	        }
	    }

	    private void robotsFactory() {
	        List<String> robotValues = new ArrayList<>();
	        robotValues.add((robotNoindex != null) ? "noindex" : "index");
	        robotValues.add((robotNofollow != null) ? "nofollow" : "follow");
	        for (int i = 0; i < robotValues.size(); i++) {
	            if (i == 0) {
	                metaRobots = robotValues.get(i);
	            } else {
	                metaRobots += ", " + robotValues.get(i);
	            }
	        }
	    }

	    private void contentURLFactory() {
	        try {
	            contentURL = currentPage.getPath();
	        } catch (NullPointerException e) {
	            contentURL = "";
	        }
	    }

	    private void canonicalFactory() {
	        try {
	            String aux = (canonical != null) ? canonical : currentPage.getPath();
	            canonical = getURL(request, resource.getResourceResolver().adaptTo(PageManager.class), aux);
	        } catch (NullPointerException ex) {
	            LOG.error(String.format("Exception: %s", ex));
	        }
	    }
	    
	    public static String getURL(SlingHttpServletRequest request, PageManager pageManager, String path) {
	        Page page = pageManager.getPage(path);
	        if (page != null) {
	            return getURL(request, page);
	        }
	        return path;
	    }

	    private static String getURL(SlingHttpServletRequest request, Page page) {
	        String vanityURL = page.getVanityUrl();
	        return StringUtils.isEmpty(vanityURL) ? request.getContextPath() + page.getPath() + ".html" : request.getContextPath() + vanityURL;
	    }

	    public static Logger getLOG() {
	        return LOG;
	    }

	    public Page getCurrentPage() {
	        return currentPage;
	    }

	    public void setCurrentPage(Page currentPage) {
	        this.currentPage = currentPage;
	    }

	    public SlingHttpServletRequest getRequest() {
	        return request;
	    }

	    public void setRequest(SlingHttpServletRequest request) {
	        this.request = request;
	    }

	    public Resource getResource() {
	        return resource;
	    }

	    public void setResource(Resource resource) {
	        this.resource = resource;
	    }

	    public String getContentURL() {
	        return contentURL;
	    }

	    public void setContentURL(String contentURL) {
	        this.contentURL = contentURL;
	    }

	    public Page getResourcePage() {
	        return resourcePage;
	    }

	    public void setResourcePage(Page resourcePage) {
	        this.resourcePage = resourcePage;
	    }

	    public String getTemplatePath() {
	        return templatePath;
	    }

	    public void setTemplatePath(String templatePath) {
	        this.templatePath = templatePath;
	    }

	    public String getMetaTitle() {
	        return metaTitle;
	    }

	    public void setMetaTitle(String metaTitle) {
	        this.metaTitle = metaTitle;
	    }

	    public String getMetaDescription() {
	        return metaDescription;
	    }

	    public void setMetaDescription(String metaDescription) {
	        this.metaDescription = metaDescription;
	    }

	    public String getMetaRobots() {
	        return metaRobots;
	    }

	    public void setMetaRobots(String metaRobots) {
	        this.metaRobots = metaRobots;
	    }

	    public String[] getMetaKeywords() {
	        return metaKeywords;
	    }

	    public void setMetaKeywords(String[] metaKeywords) {
	        this.metaKeywords = metaKeywords;
	    }

	    public String getCanonical() {
	        return canonical;
	    }

	    public void setCanonical(String canonical) {
	        this.canonical = canonical;
	    }

	    public String getRobotNoindex() {
	        return robotNoindex;
	    }

	    public void setRobotNoindex(String robotNoindex) {
	        this.robotNoindex = robotNoindex;
	    }

	    public String getRobotNofollow() {
	        return robotNofollow;
	    }

	    public void setRobotNofollow(String robotNofollow) {
	        this.robotNofollow = robotNofollow;
	    }

	    public String getCustomMetaKeywords() {
	        return customMetaKeywords;
	    }

	    public void setCustomMetaKeywords(String customMetaKeywords) {
	        this.customMetaKeywords = customMetaKeywords;
	    }

}
