package com.aem.edp.site.core.models.mobility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.edp.site.core.beans.StepBean;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class MobilityStepsModel {
	private static final Logger LOG = LoggerFactory.getLogger(MobilityStepsModel.class);
	
	@Self
	private SlingHttpServletRequest request;

	/*@Inject
	@Default(values = StringUtils.EMPTY)
	private static String type;*/
	
	private String type;
	
	private String categoryTitle;
	
	private List<StepBean> lstStepBean = new ArrayList<>();
	
	@PostConstruct
	protected void init() {
		LOG.debug("Init - MobilityStepsModel Post Construct");
		List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
		this.type = selectors.get(0);
		this.categoryTitle = selectors.get(1);
		if(type.equals("type1")) {
			List<String> lstSelect1 = new ArrayList<>();
			lstSelect1.add("0-10 metros");
			lstSelect1.add("10-20 metros");
			lstSelect1.add("20-30 metros");
			lstSelect1.add("30-40 metros");
			lstSelect1.add("40-50 metros");
			lstSelect1.add("50-60 metros");
			lstSelect1.add("60-70 metros");
			StepBean step1 = new StepBean("Distancia desde el cuadro de protecciones de la vivienda hasta la plaza de garaje", lstSelect1);
			lstStepBean.add(step1);	
		}		
		if(type.equals("type2")) {
			List<String> lstSelect2 = new ArrayList<>();
			lstSelect2.add("-3");
			lstSelect2.add("-2");
			lstSelect2.add("-1");
			lstSelect2.add("0");
			lstSelect2.add("1");
			lstSelect2.add("2");
			lstSelect2.add("3");
			StepBean step2 = new StepBean("¿En qué planta está el contador?", lstSelect2);
			lstStepBean.add(step2);	
		}
		
		
		LOG.debug("End - MobilityStepsModel Post Construct");
	}

	public String getType() {return type;}
	
	public String getCategoryTitle() {return categoryTitle;}

	public List<StepBean> getLstStepBean() {return lstStepBean;}

	public void setLstStepBean(List<StepBean> lstStepBean) {this.lstStepBean = lstStepBean;}

}
