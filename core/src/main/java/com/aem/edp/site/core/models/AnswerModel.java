package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.Resource;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AnswerModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(AnswerModel.class);
	
	@Self
	private SlingHttpServletRequest request;
	
	
	@Inject
	@Via("resource")
	private String question;
	
	/**
	 * 
	 */
	private List<String> list = new ArrayList<>();
	
	
	@PostConstruct
	protected void init() {
	
	}


	public String getQuestion() {
		return question;
	}


	public void setQuestion(String question) {
		this.question = question;
	}

	

	
	

}
