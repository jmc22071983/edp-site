package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class SliderHighlightedModel {
	private static final Logger LOG = LoggerFactory.getLogger(SliderHighlightedModel.class);
	
	private static final String TAB = "tab-";
	
	private static final String DEFAULT_TITLE = "title-";
	
	@Inject
	@Via("resource")
	private String title;
	
	@Inject
	@Via("resource")
	private String description;
	
	@Self
	private SlingHttpServletRequest request;

	private List<Map<String,String>> lstMaps = new ArrayList<>();
	
	@PostConstruct
	protected void init() {
		LOG.debug("Init - SliderHighlightedModel Post Construct");
		Resource r = request.getResource();
		if (r.hasChildren() && r.getChild("slider") != null && null != r.getChild("slider").getChildren()) {
			Iterator<Resource> it = r.getChild("slider").getChildren().iterator();
			int k = 1;
			while(it.hasNext()) {
				Resource rCategory = it.next();
				ValueMap vMap = rCategory.getValueMap();
				final String stitle = null != vMap.get("stitle") ? (String)vMap.get("stitle") : DEFAULT_TITLE.concat(String.valueOf(k));
				final String iconClass = stitle.replace(" ", "-").toLowerCase();
				final String spath = null != vMap.get("spath") ? (String)vMap.get("spath") : null;
				final String sdescription = null != vMap.get("sdescription") ? (String)vMap.get("sdescription") : null;
				final String ipath = null != vMap.get("ipath") ? (String)vMap.get("ipath") : null;
				final String ititle = null != vMap.get("ititle") ? (String)vMap.get("ititle") : null;
				
				if(StringUtils.isNotEmpty(stitle) 
						|| StringUtils.isNotEmpty(spath)
						|| StringUtils.isNotEmpty(sdescription) 
						|| StringUtils.isNotEmpty(ipath)
						|| StringUtils.isNotEmpty(ititle)) {
					
					Map<String,String> map = new HashMap<>();
					map.put("id", TAB.concat(String.valueOf(k)));
					map.put("stitle", stitle);
					map.put("iconClass", iconClass);
					map.put("spath", spath);
					map.put("sdescription", sdescription);
					map.put("ipath", ipath);
					map.put("ititle", ititle);
					lstMaps.add(map);
					k++;
				}
			}
		}	
		LOG.debug("End - SliderHighlightedModel Post Construct");
	}

	public List<Map<String, String>> getLstMaps() {return lstMaps;}

	public String getTitle() {return title;}

	public String getDescription() {return description;}


	

	
	
	
}
