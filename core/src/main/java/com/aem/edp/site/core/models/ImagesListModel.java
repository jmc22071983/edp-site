package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.edp.site.core.beans.ListImgBean;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class ImagesListModel {
	private static final Logger LOG = LoggerFactory.getLogger(ImagesListModel.class);
	
	
	@Inject
	@Via("resource")
	private String title;
	
	@Inject
	@Via("resource")
	private String description;
	
	@Inject
	@Via("resource")
	private String columns;
	
	@Self
	private SlingHttpServletRequest request;

	private List<ListImgBean> lstImgs= new ArrayList<>();
	
	@PostConstruct
	protected void init() {
		LOG.debug("Init - ImagesListModel Post Construct");
		Resource r = request.getResource();
		if (r.hasChildren() && r.getChild("imglist") != null && null != r.getChild("imglist").getChildren()) {
			Iterator<Resource> it = r.getChild("imglist").getChildren().iterator();
			while(it.hasNext()) {
				Resource rCategory = it.next();
				ValueMap vMap = rCategory.getValueMap();
				final String path = null != vMap.get("path") ? (String)vMap.get("path") : null;
				final String title = null != vMap.get("title") ? (String)vMap.get("title") : null;
				final String text = null != vMap.get("text") ? (String)vMap.get("text") : null;
				LOG.debug("Image path: {} , Image title: {}", path, title);
				if(StringUtils.isNotEmpty(path) || StringUtils.isNotEmpty(title)) {
					ListImgBean imgItem = new ListImgBean(path, title, text);
					lstImgs.add(imgItem);
				}
			}
		}	
		LOG.debug("End - ImagesListModel Post Construct");
	}


	public List<ListImgBean> getLstImgs() {return lstImgs;}

	public String getTitle() {return title;}

	public String getDescription() {return description;}


	public String getColumns() {return columns;}

	
	
	
}
