package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.Resource;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SummaryModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(SummaryModel.class);
	
	@Self
	private SlingHttpServletRequest request;
	
	
	@Inject
	@Via("resource")
	private String levelnum;
	
	private List<String> list = new ArrayList<>();
	
	
	@PostConstruct
	protected void init() {
		if(StringUtils.isNotEmpty(levelnum)) {
			int elements = Integer.parseInt(levelnum);
			String[] aElem = new String[elements];
			list = Arrays.asList(aElem);
		}
	}

	public String getLevelnum() {
		return levelnum;
	}

	public void setLevelnum(String levelnum) {
		this.levelnum = levelnum;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	
	

}
