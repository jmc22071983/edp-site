package com.aem.edp.site.core.models.general;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.edp.site.core.models.ImagesListModel;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PedagogicalImage {
	
	private static final Logger LOG = LoggerFactory.getLogger(ImagesListModel.class);
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	@Via("resource")
	private String path;
	
	private List<String> lstPedagogicalItems= new ArrayList<>();
	
	@PostConstruct
	protected void init() {
		LOG.debug("Init PedagogicalImage");
		Resource r = request.getResource();
		if (r.hasChildren() && r.getChild("list") != null && null != r.getChild("list").getChildren()) {
			Iterator<Resource> it = r.getChild("list").getChildren().iterator();
			while(it.hasNext()) {
				Resource rCategory = it.next();
				ValueMap vMap = rCategory.getValueMap();
				final String text = null != vMap.get("text") ? (String)vMap.get("text") : null;
				if(StringUtils.isNotEmpty(text)) {
					lstPedagogicalItems.add(text);
				}
			}
		}	
		LOG.debug("End PedagogicalImage");
	}

	public List<String> getLstPedagogicalItems() { return lstPedagogicalItems;}

	public String getPath() {return path;}
	
	

}
