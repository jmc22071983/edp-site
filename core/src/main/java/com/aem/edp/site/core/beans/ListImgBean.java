package com.aem.edp.site.core.beans;

public class ListImgBean {
	
	
	private String path;
	
	private String title;
	
	private String text;

	public ListImgBean(String path, String title, String text) {
		super();
		
		this.path = path;
		this.title = title;
		this.text = text;
	}
	
	public String getPath() {return path;}

	public String getTitle() {return title;}

	public String getText() {return text;}
	
	
	
}
