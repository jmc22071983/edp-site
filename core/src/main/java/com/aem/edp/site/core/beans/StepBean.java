package com.aem.edp.site.core.beans;

import java.util.ArrayList;
import java.util.List;

public class StepBean {

	private String title;
	
	private List<String> select = new ArrayList<>();;

	
	public StepBean(String title, List<String> select) {
		super();
		this.title = title;
		this.select = select;
	}

	public String getTitle() {
		return title;
	}


	public List<String> getSelect() {
		return select;
	}
	
	
}
