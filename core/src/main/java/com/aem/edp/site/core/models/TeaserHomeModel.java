package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.Resource;

import com.adobe.cq.HelloService;
import com.adobe.cq.service.ServiceTest;


@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TeaserHomeModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(TeaserHomeModel.class);

	@Self
	private SlingHttpServletRequest request;
	
	
	@Inject
	@Via("resource")
	private Resource fileReference;
	
	@Inject
	@Via("resource")
	private String title;
	
	
	@Inject
	@Via("resource")
	@Named("title2")
	private String titleRecuperadoJava;

	@Inject
	@Via("resource")
	private String description;
	
	@Inject
	@Via("resource")
	private String ftitle;
	
	@Inject
	@Via("resource")
	private String btitle;

	
	@PostConstruct
	protected void init() {
		LOG.debug("Init TeaserHomeModel");
		LOG.debug("Pintamos el title2 {}: ", titleRecuperadoJava);
		LOG.debug("Pintamos el ftitle {}: ", this.ftitle);
		LOG.debug("Pintamos el title {}: ", this.title);
		LOG.debug("Pintamos el description {}: ", this.description);
		LOG.debug("End TeaserHomeModel");
	
	}

	public Resource getFileReference() {
		return fileReference;
	}

	public void setFileReference(Resource fileReference) {
		this.fileReference = fileReference;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public String getFtitle() {
		return ftitle;
	}


	public void setFtitle(String ftitle) {
		this.ftitle = ftitle;
	}


	public String getBtitle() {
		return btitle;
	}

	public void setBtitle(String btitle) {
		this.btitle = btitle;
	}

	public String getTitleRecuperadoJava() {
		return titleRecuperadoJava;
	}

}
