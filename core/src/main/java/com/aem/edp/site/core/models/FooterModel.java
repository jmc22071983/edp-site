package com.aem.edp.site.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class)
public class FooterModel {
	private static final Logger LOG = LoggerFactory.getLogger(FooterModel.class);
    @Inject
    @Optional
    private String fileReference;
    	
	@Inject
	@Optional
	public Resource links;

	@Inject
	@Optional
	public List<Resource> certifications;
	
	@Inject
	@Optional
	public String altlogo;

	
	private List<Map<String,String>> imgList = new ArrayList<>();
	

	@PostConstruct
	protected void init() {
		if(!certifications.isEmpty()) {
			for(Resource r : certifications) {
				String certImagesPath = r.getPath();
				LOG.debug("path: {}", certImagesPath);
				ValueMap map = r.getValueMap();
				Map<String,String> certImagesMap= new HashMap<>();
				certImagesMap.put("url", (String)map.get("certimgs"));
				if(null != map.get("certimgs") && StringUtils.isNotEmpty((String)map.get("alt"))) {
					certImagesMap.put("alt", (String)map.get("alt"));
				}
				imgList.add(certImagesMap);
			}
		}	
	}

	public String getFileReference() {
		return fileReference;
	}

	public Resource getLinks() {
		return links;
	}

	public List<Resource> getCertifications() {
		return certifications;
	}

	public void setCertifications(List<Resource> certifications) {
		this.certifications = certifications;
	}


	public String getAltlogo() {
		return altlogo;
	}

	public void setAltlogo(String altlogo) {
		this.altlogo = altlogo;
	}

	public List<Map<String, String>> getImgList() {
		return imgList;
	}

	public void setImgList(List<Map<String, String>> imgList) {
		this.imgList = imgList;
	}

}
